<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LoadView extends CI_Controller {
	
	public function index() {

		$this->data['active']          		  = 'home';
		$this->data['active_submenu']  		  = '';
		

		$this->data['title_formulario_1']     = 'MARQUE UMA';
		$this->data['title_formulario_2']     = ' CONSULTA';

		$this->load->view('index', $this->data);
		
	}

	public function construcao() {		

		$this->load->view('construcao');
		
	}

	public function solucoes() {

		$this->data['active']          		  = 'so';
		$this->data['active_submenu']  		  = '';
		

		$this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

		$this->load->view('solucoes', $this->data);
		
	}


	public function quemsomos() {

		$this->data['active']          = 'qs';
        $this->data['active_submenu']  = '';

        $this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

        
		$this->load->view('quemSomos', $this->data);
		
	}  

	public function tratamentos() {

		$this->data['active']          = 'tra';
        $this->data['active_submenu']  = '';

        $this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

        
		$this->load->view('tratamentos', $this->data);
		
	}  

	public function clinicas() {

		$this->data['active']          = 'cli';
        $this->data['active_submenu']  = '';

        $this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

        
		$this->load->view('clinicas', $this->data);
		
	}

	public function quiz() {

		$this->data['active']          = '';
        $this->data['active_submenu']  = '';

        $this->data['active_trat']     = '';
        $this->data['active_serv']     = '';

		$this->load->view('quiz', $this->data);
		
	}

	
	public function blog() {

		$this->data['active']          = 'blog';
        $this->data['active_submenu']  = '';

        $this->data['active_trat']     = '';
        $this->data['active_serv']     = '';

		$this->load->view('blog', $this->data);
		
	}


	public function blogDetail($cat,$post) {


		$this->data['active']          = 'blog';
        $this->data['active_submenu']  = '';
        $this->data['post_id']  = url_to_id($post);

        $this->data['active_trat']     = '';
        $this->data['active_serv']     = '';

		$this->load->view('blogDetail', $this->data);
		
	}

	
    public function termosCondicoes() {

    	$this->data['active']          = '';
		$this->data['active_submenu']  = '';

		$this->data['active_trat']     = '';
        $this->data['active_serv']     = '';
		
		$this->load->view('termosCondicoes', $this->data);
		
	}

	public function obrigado() {

    	$this->data['active']          = '';
		$this->data['active_submenu']  = '';

		$this->data['active_trat']     = '';
        $this->data['active_serv']     = '';
		
		$this->load->view('obrigado', $this->data);
		
	}
	

}
