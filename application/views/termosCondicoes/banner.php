<!--================Slider Home Area =================-->
<!-- REVO SLIDER FULLSCREEN 1 -->
<?php
    
    $tablet  = false;
    $mobil   = false;
    $desktop = false;
    $data_y_tex_1_slide_1  = 0;
    $data_y_butom_slide_1  = 0;

    $data_y_butom_slide_2  = 0;

    $data_y_tex_1_slide_3  = 0;
    $data_y_butom_slide_3  = 0;

    $data_y_butom_slide_4  = 0;


    $position = 'left';


    if(isTablet()){
        $tablet  = true;
    }
    if(isMobile()){
        $mobil   = true;
        $position = 'center';
    }
    if($tablet == false && $mobil== false){
        $desktop = true;
    }

    if($tablet  == true){

        //echo "Olá, eu sou um tablet";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 495;

        $data_y_tex_1_slide_2  = 0;
        $data_y_butom_slide_2  = 460;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 495;

        $data_y_butom_slide_4  = 460;

    }else if($mobil  == true){

        //echo "Olá, eu sou um mobil";
        $data_y_tex_1_slide_1  = 510;
        $data_y_butom_slide_1  = 650;

        $data_y_butom_slide_2  = 600;

        $data_y_tex_1_slide_3  = 510;
        $data_y_butom_slide_3  = 650;

        $data_y_butom_slide_4  = 600;

    }elseif ($desktop == true) {
        
        //echo "Olá, eu sou um computador";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 480;

        $data_y_butom_slide_2  = 450;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 480;

        $data_y_butom_slide_4  = 450;
    }


?>
<!-- Home 50% 0-->
<?php
  echo funGetAdvancedBanners('termos_condicoes_banner_topo', '

      <main class="main main-inner main-about bg-custom" data-stellar-background-ratio="0.8" style="background: url({{img}}) 50% 0 no-repeat; background-size: cover; background-position: bottom;">
        <div class="container">
          <div class="opener">
            <h1 class="wow fadeInUp title-banner-qs">{{title}}</h1>
            <!--<p class="lead wow fadeInUp">{{subtext}}</p>-->

            <!--<div class="row">
              <header class="text-center col-md-12 col-md-offset-2- mt-50-" style="display: flex; justify-content: center;">
                <a href="{{callAction}}" data-toggle="modal-" class="btn btn-violet- btn-red wow swing" style="visibility: visible; animation-name: swing;">{{callTitle}}</a>
              </header>
            </div>-->

          </div>
        </div>
      </main>
                                                
  ');
?>    