<section class="meet_team_area pt-quiz-container" id="sessao-1">
                <div class="container" style="/*background: white;*/">
                    <div class="team_inner">
                        
                        <div class="main_title white pt-quiz-title-container mt-100">
                            <h2 class="text-left-mobil" style="color: #e31b1c;">São perguntas rápidas. Responda a todas elas e no final receba no seu email o resultado do jogo.</h2>        
                        </div>

                        <form class="formQuiz mb-quiz-form-mobil" id="formulario-quiz-implanta">

                          <input type="hidden" name="token_rdstation" value="1bdf5aee9caf657d422422327ad6bdd3">
                          <input type="hidden" name="identificador" value="formulario-quiz-implanta">
                          <input type="hidden" name="_is" value="6">
                          <input type="hidden" name="redirect_to" value="">
                          <input type="hidden" name="c_utmz" value="">
                          <input type="hidden" name="traffic_source" value="">
                          <input type="hidden" name="client_id" value="">
                          <input type="hidden" name="perfil" value="">
                          <input type="hidden" name="message"
                              value="Obrigado por preencher o nosso quiz. Irá receber o seu resultado por email!">
                          <input type="hidden" name="outro" value="Outro país">
                          <input type="hidden" name="origem" value="Site Implanta Swiss">
                          <input type="hidden" name="país" value="Brasil">

                          <div class="div-pergunta" style="margin-top: 40px;">
                            <h4 class="text-left-mobil">1/8</h4>
                            <h2 class="font-zise-question" style="margin-bottom: 40px;">Quantas vezes por dia você escova os seus dentes?</h2>                      
                            <input type="radio" name="uno" class="uno" value="1" data-texto="Apenas 1x ao dia" required="required"/> Apenas 1x ao dia<br />
                            <input type="radio" name="uno" class="uno" value="2" data-texto="Após cada refeição" required="required"/> Após cada refeição<br />
                            <input type="radio" name="uno" class="uno" value="0" data-texto="Não escovo os dentes" required="required"/> Não escovo os dentes<br />
                            <div id="p1" class="mensagem">
                              <p>A recomendação profissional é que os dentes sejam escovados após cada refeição de forma a evitar o mau hálito, as cáries e eliminar as bactérias e restos de comida que ficam entre os dentes e que são causadores de diversas doenças orais.</p>
                            </div>
                          </div>

                          <div class="div-pergunta" style="margin-top: 60px;">
                            <h4 class="text-left-mobil">2/8</h4>
                            <h2 class="font-zise-question" style="margin-bottom: 40px;">Tem o hábito de fumar regularmente?</h2>                      
                            <input type="radio" name="dos"  class="dos" value="2" data-texto="Não fumo" required="required"/> Não fumo<br />
                            <input type="radio" name="dos"  class="dos" value="1" data-texto="Fumo eventualmente" required="required"/> Fumo eventualmente<br />
                            <input type="radio" name="dos"  class="dos" value="0" data-texto="Fumo muito, todos os dias" required="required"/> Fumo muito, todos os dias<br />
                            <div id="p2" class="mensagem">
                              <p>Se não é fumador, parabéns! Nunca é demais falar: o fumo é uma das causas do cancro oral, além de prejudicar todo o nosso corpo com as suas toxinas, escurecer os dentes e causar halitose. Ao fumar afeta a sua saúde oral e também prejudica a reabilitação em caso de cirurgias da boca. Procure ajuda profissional se percebe que o fumo está a afetar os seus dentes. Evite fumar e faça mais pela sua saúde oral.</p>
                            </div>
                          </div>

                          <div class="div-pergunta" style="margin-top: 40px;">
                            <h4 class="text-left-mobil">3/8</h4>
                            <h2 class="font-zise-question" style="margin-bottom: 40px;">Você sente sensibilidade nos dentes ao ingerir alimentos quentes ou gelados?</h2>                      
                            <input type="radio" name="tres" class="tres" value="0" data-texto="Sim" required="required"/> Sim<br />
                            <input type="radio" name="tres" class="tres" value="2" data-texto="Não" required="required"/> Não<br />
                            <input type="radio" name="tres" class="tres" value="1" data-texto="Talvez" required="required"/> Talvez<br />
                            <div id="p3" class="mensagem">
                              <p>A sensibilidade dentária é um fator de atenção. Caso sinta algum desconforto durante a mastigação deve procurar o seu dentista para avaliar se tem algum problema dentário e assim poder tratá-lo antes que se agrave. Se não sente nenhum desconforto continue a cuidar bem do seu sorriso!</p>
                            </div>
                          </div>

                          <div class="div-pergunta" style="margin-top: 60px;">
                            <h4 class="text-left-mobil">4/8</h4>
                            <h2 class="font-zise-question" style="margin-bottom: 40px;">Sofre de perda dentária?</h2>                      
                            <input type="radio" name="cuatro"  class="cuatro" value="2" data-texto="Não, tenho todos os dentes" required="required"/> Não, tenho todos os dentes<br />
                            <input type="radio" name="cuatro"  class="cuatro" value="1" data-texto="Sim, faltam-me até 2 dentes" required="required"/> Sim, faltam-me até 2 dentes<br />
                            <input type="radio" name="cuatro"  class="cuatro" value="0" data-texto="Sim, faltam-me vários dentes" required="required"/> Sim, faltam-me vários dentes<br />
                            <div id="p4" class="mensagem">
                              <p>Se tem todos os dentes saudáveis, parabéns! Continue a cuidar bem da sua saúde oral. Em caso de perda dentária, saiba que a falta de dentes causa sérios problemas mastigatórios e funcionais além de afetar a autoestima. A falta de apenas um dente é suficiente para gerar um conjunto de outros problemas, prejudicando toda a sua dentição. Consulte o seu dentista – ele é a pessoa mais indicada para avaliar e tratar da sua saúde oral.</p>
                            </div>
                          </div>

                          <div class="div-pergunta" style="margin-top: 40px;">
                            <h4 class="text-left-mobil">5/8</h4>
                            <h2 class="font-zise-question" style="margin-bottom: 40px;">Visita regularmente o seu dentista?</h2>                      
                            <input type="radio" name="cinco" class="cinco" value="2" data-texto="Sim, duas ou mais vezes por ano" required="required"/> Sim, duas ou mais vezes por ano<br />
                            <input type="radio" name="cinco" class="cinco" value="1" data-texto="Sim, uma vez por ano" required="required"/> Sim, uma vez por ano<br />
                            <input type="radio" name="cinco" class="cinco" value="0" data-texto="Não" required="required"/> Não<br />
                            <div id="p5" class="mensagem">
                              <p>Se a sua resposta for não, saiba que a prevenção é a melhor forma de evitar problemas dentários. Deve visitar o seu dentista no mínimo de 6 em 6 meses e não apenas quando sente dor. Proteja a saúde dos seus dentes e boca através de bons hábitos e cuidados constantes.</p>
                            </div>
                          </div>

                          <div class="div-pergunta" style="margin-top: 60px;">
                            <h4 class="text-left-mobil">6/8</h4>
                            <h2 class="font-zise-question" style="margin-bottom: 40px;">A cárie é a 2ª doença mais comum do mundo. Sofre deste problema?</h2>                      
                            <input type="radio" name="seis"  class="seis" value="2" data-texto="Não" required="required"/> Não<br />
                            <input type="radio" name="seis"  class="seis" value="1" data-texto="Talvez" required="required"/> Talvez<br />
                            <input type="radio" name="seis"  class="seis" value="0" data-texto="Sim" required="required"/> Sim<br />
                            <div id="p6" class="mensagem">
                              <p>A cárie dentária é uma das principais causas da perda dentária precoce, que por sua vez, é umas consequências mais graves dos problemas orais.</p>
                            </div>
                          </div>

                          <div class="div-pergunta" style="margin-top: 40px;">
                            <h4 class="text-left-mobil">7/8</h4>
                            <h2 class="font-zise-question" style="margin-bottom: 40px;">Usa prótese dentária?</h2>                      
                            <input type="radio" name="siete" class="siete" value="0" data-texto="Sim, uso prótese removível" required="required"/> Sim, uso prótese removível<br />
                            <input type="radio" name="siete" class="siete" value="1" data-texto="Sim, uso prótese fixa" required="required"/> Sim, uso prótese fixa<br />
                            <input type="radio" name="siete" class="siete" value="2" data-texto="Não" required="required"/> Não<br />
                            <div id="p7" class="mensagem">
                              <p>A prótese dentária fixa é uma solução preferível à prótese dentária removível quer pela vantagem funcional e estética, quer pela segurança na mastigação e qualidade no conforto.</p>
                            </div>
                          </div>

                          <div class="div-pergunta" style="margin-top: 60px;">
                            <h4 class="text-left-mobil">8/8</h4>
                            <h2 class="font-zise-question" style="margin-bottom: 40px;">Já alguma vez efetuou algum tipo de tratamento com implantes?</h2>                      
                            <input type="radio" name="ocho"  class="ocho" value="0" data-texto="Sim, na Swiss Dental Services" required="required"/> Sim, na Swiss Dental Services<br />
                            <input type="radio" name="ocho"  class="ocho" value="1" data-texto="Sim, em utra clínica" required="required"/> Sim, em utra clínica<br />
                            <input type="radio" name="ocho"  class="ocho" value="2" data-texto="Não, nunca efetuei nenhum tratamento" required="required"/> Não, nunca efetuei nenhum tratamento<br />
                            <div id="p8" class="mensagem">
                              <p>A reabilitação oral através da colocação de implantes dentários devolve-lhe a qualidade de vida e autoestima. Na Swiss Dental Services, estas duas vantagens que trazemos aos nossos pacientes são a razão pela qual trabalhamos todos os dias e contamos já com 30.000 casos de sucesso.</p>
                            </div>
                          </div>

                          <div class="container-quiz mb-60" style="margin-top: 60px;">


                            <div class="row">

                              <div class="col-sm-5">
                                
                                <div class="bg-box">

                                  <div class="row">

                                    <p class="title-quiz-form">Fique sabendo o seu resultado aqui abaixo</p>
                                    
                                  </div>
                                  
                                  <div class="row imput-quiz-">

                                    <div class="col-sm-12">                                
                                      <input type="text"  name="name"  class="form-control" placeholder="Nome *" required>
                                    </div>

                                  </div>
                                  <br>
                                  <div class="row imput-quiz-">

                                    <div class="col-sm-12">                               
                                      <input type="email" name="email"  class="form-control"  placeholder="E-mail *" required>
                                    </div>

                                  </div>                            

                                  <div class="row imput-quiz-" style="margin-top: 40px;">

                                    <div class="row" style="display: flex; justify-content: center;">                               
                                       <button class="btn btn-primary btn-quiz" style="background-color: #e31b1c; border-color: #CF1F25; " id="btn-jogo">Enviar</button>
                                    </div>                            
                                    
                                  </div>

                                </div>

                              </div>
                              
                            </div>

                          
                          </div>
                          
                        </form>                  

                    </div>                            

                    
                </div>
            </section>