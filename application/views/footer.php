<!-- Footer -->

      <footer id="footer" class="footer bg-dark- bg-footer-img">     
        <div class="container">
          <div class="row-base row">
            <div class="brand-info col-base col-sm-6 col-md-4">
              <img src="<?php base_url('logo_prid-02.svg','img/custon') ?>" class="marcas-mov">
              <div class="move-text">
                <p>+55 (48) 98844-7887 </p>
                <p>contato@prid.com.br</p>                
              </div>
            </div>
            <div class="col-base col-sm-6 col-md-3">
              <p style="font-family: 'Montserrat', sans-serif;">NAVEGAÇÃO</p><br>
              <ul class="nav-bottom">
                <li><a href="<?php base_url('') ?>">Home</a></li>
                <li><a href="<?php base_url('solucoes-oferecidas') ?>">Soluções Oferecidas</a></li>
                <li><a href="<?php base_url('quem-somos') ?>">Quem Somos</a></li>
                <li><a href="<?php base_url('tratamentos') ?>">Tratamentos</a></li>
                <li><a href="<?php base_url('blog') ?>">Blog</a></li>
                <li><a href="<?php base_url('clinicas') ?>">Clínicas</a></li>
              </ul>
            </div>
            <div class="col-contacts col-base col-sm-6 col-md-3">
              <p>ÓRGÃOS REGULADORES</p><br>
              <ul class="nav-bottom">
                <li><a href="https://www.gov.br/anvisa/pt-br" target="_blank"> Anvisa - Agência Nacional de Vigilância Sanitária</a></li>
                <li><a href="https://website.cfo.org.br/" target="_blank"> CFO - Conselho Federal de Odontologia</a></li>
                <li><a href="https://www.gov.br/ans/pt-br" target="_blank"> ANS - Agência Nacional de Saúde Suplementar</a></li>
                
              </ul>
            </div>
            
          </div>
        </div>
        <div class="container">
          <hr>

          <div class="row marcas-footer">

            <div class="col-base col-sm-4 col-md-2">
                <a href="https://lessendentalclinic.com.br/" target="_blank"><img src="<?php base_url('logo_lessendental-02.png','img/custon') ?>" class="marca-lessen"></a> 
            </div>

            <div class="col-base col-sm-4 col-md-2">
                <a href="https://transformeseusorriso.com.br/" target="_blank"><img src="<?php base_url('logos_moviments-01.png','img/custon') ?>" class="marcas-mov"></a> 
            </div>

            <div class="col-base col-sm-4 col-md-2">
                <a href="https://sourrisos.com.br/" target="_blank"><img src="<?php base_url('logos_moviments-02.png','img/custon') ?>" class="marcas-mov"></a> 
            </div>

            <div class="col-base col-sm-4 col-md-2">
                <a href="https://belezanosorrir.com.br/" target="_blank"><img src="<?php base_url('logos_moviments-03.png','img/custon') ?>" class="marcas-mov"></a> 
            </div>

            
          </div>


        </div>
        <div class="footer-bottom">
          <div class="container footer-copy-riht">
              <div class="copyrights footer-title-copy-right">
              © PRID. All Rights Reserved
              </div>
              <ul class="social-list">
                <li><a href="http://facebook.com/PRID-Brasil-Programa-Regional-de-Implantes-Dent%C3%A1rios-102009648715964" target="_blank" class="fa fa-facebook"></a></li>
                <li><a href="https://twitter.com/pridbrasil" target="_blank" class="fa fa-twitter"></a></li>
                <li><a href="https://www.youtube.com/channel/UCK6HJUJ1pqqxUuys9VjKlQg" target="_blank" class="fa fa-youtube"></a></li>
                <li><a href="https://www.instagram.com/prid_brasil/" target="_blank" class="fa fa-instagram"></a></li>
              </ul>  
          </div>
        </div>
      </footer>


      <!-- Messenger Plugin de bate-papo Code -->
      <div id="fb-root"></div>

      <!-- Your Plugin de bate-papo code -->
      <div id="fb-customer-chat" class="fb-customerchat">
      </div>