
    <!--********************************************* 5ta sessao *********************************************************************************************-->  


<section class="section clearfix">
  <div class="left-fluid-content col-sm-12 col-md-6 col-lg-4 col-lg-offset-2">
    <?php
      echo funGetAdvancedBanners('quem_somos_five', '

        <header class="text-center">
          <h2 class="text-left section-title-3">{{title}}</h2>
        </header>
        <div class="about-entry">
          <p>{{text}}</p>
          <p>{{subtext}}</p>
        </div>

        <!--<div class="row">
          <header class="text-center col-md-8 col-md-offset-2- mt-50" style="display: flex;">
            <a href="{{callAction}}" data-toggle="modal-" class="btn btn-violet- btn-red wow swing" style="visibility: visible; animation-name: swing;">{{callTitle}}</a>
          </header>
        </div> -->       

      ');
    ?>
    
  </div>
  <div class="bg-aside-1- bg-aside-quem-somos bg-right-fluid col-md-6 hidden-sm hidden-xs"></div>
</section>