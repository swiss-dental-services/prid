<!--****************************************************** 2da sessao  *************************************************************-->

<section class="statistics bg-light section">
  <div class="container">
    <div class="row">
      <header class="text-center col-md-8 col-md-offset-2">
      <h2 class="section-title wow fadeInLeft"><?=quem_somos_two_text_1?></h2>
      <p class="section-lead wow fadeInRight"><?=quem_somos_two_text_2?></p>
      </header>
    </div>
    <div class="section-content">
      <div class="row-base row">
        <div class="col-stat col-base col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="0.0s">
          <div class="stat-number" data-value="45">45</div>
          <?=quem_somos_two_text_4?>
        </div>
        <div class="col-stat col-base col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="0.1s">
          <div class="stat-number" data-value="11">11</div>
          <?=quem_somos_two_text_5?>
        </div>
        <div class="col-stat col-base col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="0.2s">
          <div class="stat-number" data-value="6">6</div>
          <?=quem_somos_two_text_6?>
        </div>
        <div class="col-stat col-base col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="0.3s">
          <div class="stat-number"  data-value="15">15</div>
          <?=quem_somos_two_text_7?>
        </div>
      </div>
      <div class="row">
        <div class="stat-descr col-md-10 col-md-offset-1  wow fadeInUp">
          <?=quem_somos_two_text_3?>
        </div>
      </div>
      
        <div class="row">
          <header class="text-center" style="display: flex;justify-content: center;">
            <a href="#formulario" data-toggle="modal-" class="btn btn-violet- btn-red wow swing smooth-scroll" style="visibility: visible; animation-name: swing;">Marcar avaliação</a>
          </header>
        </div>
      
    </div>
  </div>
</section>