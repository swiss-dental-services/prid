
    <!--****************************************************** 7ma sessao  ****************************************************************************-->

<section class="section clearfix pt-2em wow fadeInUp" style="/*background-color: #f7f7f7;*/">
  <div class="left-fluid-content col-sm-12 col-md-6 col-lg-4 col-lg-offset-2">
    <?php
      echo funGetAdvancedBanners('solucoes_seven_a', '

        <header class="text-center">
          <h2 class="text-left section-title-3">{{title}}</h2>
        </header>
        <div class="about-entry">
          <p>{{text}}</p>
          <p>{{subtext}}</p>
          <p style="color: #e31b1c">{{subtitle}}</p>
        </div>

        <div class="row">
          <header class="text-center col-md-8 col-md-offset-2- mt-50" style="display: flex;">
            <a href="{{callAction}}" data-toggle="modal-" class="btn btn-violet- btn-red wow swing" style="visibility: visible; animation-name: swing;">{{callTitle}}</a>
          </header>
        </div>       

      ');
    ?>
    
  </div>
  <div class="bg-aside-1- bg-aside-solucoes-seven_a bg-right-fluid col-md-6 hidden-sm hidden-xs"></div>
</section>


<section class="section clearfix pt-2em wow fadeInUp" style="padding-bottom: 10em;">
  <div class="left-fluid-content col-sm-12 col-md-6 col-lg-4 col-lg-offset-2 move-div-right">
    <?php
      echo funGetAdvancedBanners('solucoes_seven_b', '

        <header class="text-center">
          <h2 class="text-left section-title-3">{{title}}</h2>
        </header>
        <div class="about-entry">
          <p>{{text}}</p>
        </div>

        <div class="row">
          <header class="text-center col-md-8 col-md-offset-2- mt-50" style="display: flex;">
            <a href="{{callAction}}" data-toggle="modal-" class="btn btn-violet- btn-red wow swing" style="visibility: visible; animation-name: swing;">{{callTitle}}</a>
          </header>
        </div>       

      ');
    ?>
    
  </div>
  <div class="bg-aside-1- bg-aside-solucoes-seven_b bg-right-fluid col-md-6 hidden-sm hidden-xs move-div-left"></div>
</section>