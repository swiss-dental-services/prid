
   <!--****************************************************** 6ta sessao Alimentação - Estética - Saúde*************************************************--> 

<section class="features bg-dark section-banner-2 section">
  <div class="container">
    <div class="row-base row">

      <?php
        echo funGetAdvancedBanners('solucoes_six', '

          <div class="col-base col-feature col-sm-6 col-md-4 wow fadeInUp">
            <div class="media-left-">  <img src="{{img}}" style="width: 15%; padding-bottom: 15px;"> </div>
            <div class="media-right">
              <h4 class="text-white">{{title}}</h4>
              <ul style="list-style-type: disc; margin-left: 27px;">
                <li>{{subtitle}}</li>
                <li>{{text}}</li>
                <li>{{subtext}}</li>
              </ul>
              
            </div>
          </div>
                                        
        ');
      ?>
      
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2 wow fadeInUp">
        <div class="banner-in-section banner banner-blue">
          <div class="banner-content">
            <h2 class="banner-title"> Transforme sua vida agora! <br> <span style="font-weight: normal;">Saiba mais. Clique aqui.</span></h2>
            <a href="#request" data-toggle="modal" class="btn btn-light- btn-amarelo wow swing">Saiba mais</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>