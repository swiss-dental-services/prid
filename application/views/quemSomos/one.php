<!--****************************************************** 1ra sessao  **********************************************************************-->
        
<section class="about section">
  <div class="container">
    <h2 class="section-title text-left text-dark wow fadeInLeft"><?=quem_somos_text_1?>  —<span class="fade-title mob-fade-title"><?=quem_somos_text_2?></span></h2>
    <div class="row-about row">
      <div class="col-md-10 col-md-offset-2 wow fadeInDown">
        <h3 class="text-dark-" style="color: #e7334b;"><?=quem_somos_text_3?><br></h3>
      </div>
    </div>
    <?php
          echo funGetAdvancedBanners('quem_somos_one', '
                    
    <div class="row-about row">
      <div class="col-md-4 col-md-offset-3">
        <p>{{text}}</p>
      </div>
      <div class="col-md-4">
        <p>{{subtext}}</p>
      </div>
    </div>
                      
      ');
    ?>
  </div>
</section>


    