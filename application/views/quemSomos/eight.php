<!--****************************************************** 8va sessao  ****************************************************************************-->
<section class="about clearfix" style="background-color: #f7f7f7;">
    <div class="bg-about- bg-quiz bg-left-fluid col-md-4">
      <div class="about-cite">
        <div class="about-cite-title">
          <p class="text-left-home"></p> 
        </div>
        
      </div>
    </div>
    <div class="col-about col-md-6 col-md-offset-4">

      <?php
        echo funGetAdvancedBanners('solucoes_eight', '

          <h2 class="about-title mb-text" style="color: #e31b1c;">{{title}}</h2>
          <p>{{text}}</p>   

          <div class="row">
            <header class="text-center col-md-8 col-md-offset-2- mt-50" style="display: flex;">
              <a href="{{callAction}}" data-toggle="modal-" class="btn btn-violet- btn-red wow swing" style="visibility: visible; animation-name: swing;">{{callTitle}}</a>
            </header>
          </div>                 

        ');
      ?>
    </div>
</section>