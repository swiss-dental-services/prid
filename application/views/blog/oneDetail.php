<section class="blog-list bg-light- section">
  <div class="container">
    <div class="row">
      <div class="primary col-md-8">

        <div id="listado">   

         <?php echo getOneNew($post_id,' 

            <h2 class="lead- text-center">{{title}}</h2>
            {{details}}

          '); ?>    
        
          
        </div>
        <!--<div class="text-center" style="display: flex; justify-content: space-around;">
          <a href="javascript: void(0)" class="btn btn-red btn-navegacao load-more" id="prev">Anterior</a>
          <a href="javascript: void(0)" class="btn btn-red btn-navegacao load-more" id="next">Próximo</a>
        </div>-->
      </div>
      <div class="secondary col-md-4">
        
        <div class="widget widget_categories">
          <h3 class="widget-title">Categorias</h3>
          <ul class="blog-cat">
            <li><a href="<?php base_url('blog') ?>">Todas</a></li>
            <li><a href="<?php base_url('blog') ?>?category=Sobre o PRID">Sobre o PRID</a></li>
            <li><a href="<?php base_url('blog') ?>?category=Sobre os Tratamentos">Sobre os Tratamentos</a></li>
            <li><a href="<?php base_url('blog') ?>?category=Mitos e Verdades">Mitos e Verdades</a></li>
            <!--<li><a href="<?php /*base_url('blog')*/ ?>?category=Dicas de Beleza">Dicas de Beleza</a></li>-->
          </ul>
        </div>
        <div class="widget widget_tags">
          <h3 class="widget-title">Posts Recentes</h3>
          <div class="blog-tags">
            <?= getNews('
              
              <div class="row" style="position: relative; margin-bottom: 40px;">
                  <div class="col-md-6" style="max-width: 30%">
                      <img width="130%" src="{{image}}" alt="">
                  </div>
                  <div class="col-md-6">
                      <a href="{{href}}" style="padding: 0px; margin: 0px; background: none;"><h4 class="posts" style="font-size: 14px; margin: 0px; letter-spacing: 0em; text-transform: none; font-weight: normal;">{{title}}</h4></a>
                  </div>                  

                  <div class="col-md-6 custon-div-1" style="/*position: absolute; top: 65px; margin-bottom: 13px;*/">
                      <h4 class="posts" style="font-size: 14px; margin: 0px; letter-spacing: 0em; text-transform: none; font-weight: normal;">{{publishDate}}</h4>
                  </div>

                  <!--<div class="col-md-6 custon-div-2" style="/*position: absolute; top: 65px; margin-bottom: 13px; right: 8px;*/">
                      <h4 class="posts" style="font-size: 14px; margin: 0px; letter-spacing: 0em; text-transform: none; font-weight: normal;">{{editor}}</h4>
                  </div>-->                  

              </div>',5); 
            ?>
           
          </div>
        </div>
      </div>
    </div>
  </div>
</section>    

          