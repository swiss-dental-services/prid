
    <!--================================================================= 1ra sessao ============================================================================-->
<?php

    $cat = isset($_GET['category']) ? $_GET['category'] : null;

?>  


<section class="blog-list bg-light- section">
  <div class="container">
    <div class="row">
      <div class="primary col-md-8">

        <div id="listado">   

         <?= getNews('

          <article class="blog lista">
            <div class="row">
              <div class="blog-thumbnail col-lg-5">
                <a href="{{href}}"><div class="blog-thumbnail-bg col-lg-5 visible-lg" style="background-image: url({{image}});"></div></a>
                <a href="{{href}}"><div class="blog-thumbnail-img hidden-lg"><img alt="" class="img-responsive" src="{{image}}"></div></a>
              </div>
              <div class="blog-info col-lg-7">
                <a href="{{href}}" class="blog-rubric">{{category}}</a>
                <h3 class="blog-title">
                  <a href="{{href}}">{{title}}</a>
                </h3>
                <p class="text-muted">{{subTitle}}</p>
                <div class="blog-meta">
                  <div class="pull-left">
                    by <a href="javascript: void(0)"  class="author">{{editor}}</a>
                  </div>
                   <div class="pull-right">
                    <div class="time">{{publishDate}}</div>
                  </div>
                </div>
              </div>
            </div>
          </article>', null, null, null, $cat); ?>     
        
          <!--<article class="blog lista">
            <div class="row">
              <div class="blog-thumbnail col-lg-5">
                <a href=""><div class="blog-thumbnail-bg col-lg-5 visible-lg" style="background-image: url(assets/img/custon/1-700x430.jpg);"></div></a>
                <a href=""><div class="blog-thumbnail-img hidden-lg"><img alt="" class="img-responsive" src="assets/img/custon/1-700x430.jpg"></div></a>
              </div>
              <div class="blog-info col-lg-7">
                <a href="" class="blog-rubric">consulting 1</a>
                <h3 class="blog-title">
                  <a href="">Branding Do You Know Who You Are</a>
                </h3>
                <p class="text-muted">Chances are unless you are very lucky you will go thru many different relationships before you find your special someone. Finding your sole mate is like gambling...</p>
                <div class="blog-meta">
                  <div class="pull-left">
                    by <a href="#"  class="author">Craig David</a>
                  </div>
                   <div class="pull-right">
                    <div class="time">July, 26</div>
                  </div>
                </div>
              </div>
            </div>
          </article>
          <article class="blog lista">
            <div class="row">
              <div class="blog-thumbnail col-lg-5">
                <a href=""><div class="blog-thumbnail-bg col-lg-5 visible-lg" style="background-image: url(assets/img/custon/1-700x430.jpg);"></div></a>
                <a href=""><div class="blog-thumbnail-img hidden-lg"><img alt="" class="img-responsive" src="assets/img/custon/1-700x430.jpg"></div></a>
              </div>
              <div class="blog-info col-lg-7">
                <a href="" class="blog-rubric">consulting 2</a>
                <h3 class="blog-title">
                  <a href="">Branding Do You Know Who You Are</a>
                </h3>
                <p class="text-muted">Chances are unless you are very lucky you will go thru many different relationships before you find your special someone. Finding your sole mate is like gambling...</p>
                <div class="blog-meta">
                  <div class="pull-left">
                    by <a href="#"  class="author">Craig David</a>
                  </div>
                   <div class="pull-right">
                    <div class="time">July, 26</div>
                  </div>
                </div>
              </div>
            </div>
          </article>
          <article class="blog lista">
            <div class="row">
              <div class="blog-thumbnail col-lg-5">
                <a href=""><div class="blog-thumbnail-bg col-lg-5 visible-lg" style="background-image: url(assets/img/custon/1-700x430.jpg);"></div></a>
                <a href=""><div class="blog-thumbnail-img hidden-lg"><img alt="" class="img-responsive" src="assets/img/custon/1-700x430.jpg"></div></a>
              </div>
              <div class="blog-info col-lg-7">
                <a href="" class="blog-rubric">consulting 3</a>
                <h3 class="blog-title">
                  <a href="">Branding Do You Know Who You Are</a>
                </h3>
                <p class="text-muted">Chances are unless you are very lucky you will go thru many different relationships before you find your special someone. Finding your sole mate is like gambling...</p>
                <div class="blog-meta">
                  <div class="pull-left">
                    by <a href="#"  class="author">Craig David</a>
                  </div>
                   <div class="pull-right">
                    <div class="time">July, 26</div>
                  </div>
                </div>
              </div>
            </div>
          </article>-->
          <h2 class="show-sem-cat">OPS, DESCULPE NÃO TEMOS POTS PARA ESSA CATEGORIA!!!!</h2>
        </div>
        <div class="text-center" style="display: flex; justify-content: space-around;" id="pagination-blog">
          <a href="javascript: void(0)" class="btn btn-red btn-navegacao load-more" id="prev">Anterior</a>
          <a href="javascript: void(0)" class="btn btn-red btn-navegacao load-more" id="next">Próximo</a>
        </div>
      </div>
      <div class="secondary col-md-4">
        <!--<div class="widget widget_search">
          <h3 class="widget-title">Search in blog</h3>
          <form class="search-form">
            <div class="input-group">
              <input type="search" class="input-round form-control" placeholder="Search" name="search">
              <span class="input-group-btn">
                <button class="btn" type="submit"><span class="arrow-right"></span></button>
              </span>
            </div>
          </form>
        </div>-->
        <div class="widget widget_categories">
          <h3 class="widget-title">Categorias</h3>
          <ul class="blog-cat">
            <li><a href="<?php base_url('blog') ?>">Todas</a></li>
            <li><a href="<?php base_url('blog') ?>?category=Sobre o PRID">Sobre o PRID</a></li>
            <li><a href="<?php base_url('blog') ?>?category=Sobre os Tratamentos">Sobre os Tratamentos</a></li>
            <li><a href="<?php base_url('blog') ?>?category=Mitos e Verdades">Mitos e Verdades</a></li>
            <!--<li><a href="<?php /*base_url('blog')*/ ?>?category=Dicas de Beleza">Dicas de Beleza</a></li>-->
          </ul>
        </div>
        <div class="widget widget_tags">
          <h3 class="widget-title">Posts Recentes</h3>
          <div class="blog-tags">
            <?= getNews('
              
              <div class="row" style="position: relative; margin-bottom: 40px;">
                  <div class="col-md-6" style="max-width: 30%">
                      <img width="130%" src="{{image}}" alt="">
                  </div>
                  <div class="col-md-6">
                      <a href="{{href}}" style="padding: 0px; margin: 0px; background: none;"><h4 class="posts" style="font-size: 14px; margin: 0px; letter-spacing: 0em; text-transform: none; font-weight: normal;">{{title}}</h4></a>
                  </div>                  

                  <div class="col-md-6 custon-div-1" style="/*position: absolute; top: 65px; margin-bottom: 13px;*/">
                      <h4 class="posts" style="font-size: 14px; margin: 0px; letter-spacing: 0em; text-transform: none; font-weight: normal;">{{publishDate}}</h4>
                  </div>

                  <!--<div class="col-md-6 custon-div-2" style="/*position: absolute; top: 65px; margin-bottom: 13px; right: 8px;*/">
                      <h4 class="posts" style="font-size: 14px; margin: 0px; letter-spacing: 0em; text-transform: none; font-weight: normal;">{{editor}}</h4>
                  </div>-->

                  

              </div>',5); 
            ?>
            <!--<a href="#">Web Design</a>
            <a href="#">Technology</a>
            <a href="#">Marketing</a>
            <a href="#">Google</a>
            <a href="{{href}}">{{title}}</a>-->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>    

                        