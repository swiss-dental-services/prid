
<!--================4ta sesao=================-->
    <section class="blog_sidebar_area" id="top-sessao-blog">
        <div class="container" style="padding-bottom: 70px">
            <div class="row row_direction">
                <div class="col-lg-8 offset-lg-1">
                    <div class="blog_side_inner" id="listado">


                        
                        <?= getNews('
                            <div class="blog_side_item lista">
                                <div class="media" style="align-items: stretch;">
                                    <div class="d-flex" style="max-width: 40%">
                                        <img width="100%" src="{{image}}" alt="">
                                    </div>
                                    <div class="media-body" style="/*display: flex;flex-wrap: wrap;align-items: center;justify-content: flex-start;align-content: space-between;*/ display: flex; flex-wrap: wrap; align-items: flex-start; justify-content: space-between; align-content: space-between; flex-direction: column;">
                                        <a class="time" style="margin-right:10px;cursor:default">{{publishDate}}</a>
                                        <a class="tag" style="margin:0;cursor:default">{{category}}</a>
                                        <a href="{{href}}"><h4>{{title}}</h4></a>
                                        <a class="ctaPattern ctaBottomLine ctaGreyC" href="{{href}}" style="padding: 0;font-size: 16px;">' . know_more . '</a>
                                    </div>
                                </div>
                            </div>', null, null, null, $cat); ?>






                    </div>
                    <nav role="navigation" class="mt-100 mb-50">
                        <ul class="cd-pagination animated-buttons custom-icons2 ml-0" id="pagination-blog">
                            <li class="button-pag pl-btn-left" id="prev"><a href="javascript: void(0)"><i>ant</i></a></li>

                            <li class="button-pag pl-btn-rigth" id="next"><a href="javascript: void(0)"><i>prox</i></a></li>
                        </ul>
                    </nav>
                    <!--<nav id="pagination" data-blogcounter="<?/*=blogCounter()*/?>" aria-label="Page navigation example" class="pagination_inner" style="display: none">
                      <ul class="pagination">
                      </ul>
                    </nav> -->
                </div>
                <!-- TRADUÇÃO   blog_categoria  -->
                <div class="col-lg-3">
                    <div class="left_sidebar_area">
                        <aside class="l_widget categories_wd">
                            <div class="l_wd_title">
                                <h3><?=blog_categoria?></h3>
                            </div>
                            <ul class="link-recents pl-0">

                                <?php
                                    /*echo funGetSlide('blog-categories','','','
                                    <li><a href="?categoria={{ctaTitle}}">{{ctaTitle}}  </a></li>
                                ');*/

                                ?>
                                <li><a href="<?php base_url('blog') ?>">Todas</a></li>
                                <li><a href="<?php base_url('blog') ?>?category=Face Swiss">Sobre a Face Swiss</a></li>
                                <li><a href="<?php base_url('blog') ?>?category=Sobre os Tratamentos">Sobre os Tratamentos</a></li>
                                <li><a href="<?php base_url('blog') ?>?category=Dicas de Beleza">Dicas de Beleza</a></li>
                                <li><a href="<?php base_url('blog') ?>?category=Mitos e Verdades">Mitos e Verdades</a></li>
                                <!--<li><a href="<?php /*base_url('blog')*/ ?>?category=Dúvidas">Dúvidas</a></li>
                                <li><a href="<?php /*base_url('blog')*/ ?>?category=Higiene">Higiene</a></li>
                                <li><a href="<?php /*base_url('blog')*/ ?>?category=Implantes">Implantes</a></li>
                                <li><a href="<?php /*base_url('blog')*/ ?>?category=Mais Saudavel">Mais Saudável</a></li>
                                <li><a href="<?php /*base_url('blog')*/ ?>?category=Saúde">Saúde</a></li> -->
                            </ul>
                        </aside>
                        <!-- TRADUÇÃO   blog_posts  -->
                        <aside class="l_widget r_post_wd">
                            <div class="l_wd_title">
                                <h3><?=blog_posts?></h3>
                            </div>
                        <?= getNews('
                            <div class="media">
                                <div class="d-flex" style="max-width: 30%">
                                    <img width="100%" src="{{image}}" alt="">
                                </div>
                                <div class="media-body">
                                    <a href="{{href}}"><h4>{{title}}</h4></a>
                                </div>
                            </div>',5); ?>






                        </aside>
                        <!-- TRADUÇÃO   blog_tags  -->
                        <aside class="l_widget tags_wd">
                            <div class="l_wd_title">
                                <!--<h3><?=blog_tags?></h3>-->
                            </div>
                            <div class="tag_list">
                                <?php
                                    /*echo funGetSlide('blog-keys','','','
                                    <a href="?keywords={{ctaTitle}}">{{ctaTitle}}  </a>
                                ');*/

                                ?>
                            </div>
                        </aside>
                        <aside class="l_widget search_wd">
                            <div class="l_wd_title">
                                <h3><?=contact_titulo2?></h3>
                            </div>
                            
                            <h2><?=contato_cidade?></h2>
                            <p><?=contato_endereco1?><br /><?/*=contato_endereco2*/?></p>
                            <!--<p><?=contato_telefone_comercial?></p>
                            <p><?=contato_correio_comercial?></p>-->
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <?php echo getOneNew($post_id,'
      <section class="image_banner_area" style="background-image: url({{image}});background-position-y:top;position:relative">
      <div class="filterBlog" style="
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: black;
        z-index: 1;
        opacity: .5;
      "></div>
        <div class="container">
          <div class="single_banner_text" style="position:relative;z-index:2">
            <div class="date">
              <a href="#">{{publishDate}}</a>
              <i class="ion-record"></i>
              <a href="#">{{editor}}</a>
            </div>
            <h3>{{title}}</h3>
          </div>
        </div>
      </section>'); ?>