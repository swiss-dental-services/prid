<div class="page-section mt-desktop-blog tabletOnly" id="form_blogg" style="">
    <div class="container move-right">
        <div class="row">
                

            <div class="col-md-6 left-50 right-desktop-blog mb-form-blog" style="display: flex; justify-content: center; align-items: center; flex-direction: column;">
                <div class="title-fs-45 mt-50 mb-50" style="line-height: 25px;">
                    <span class="bold uppercase">Newsletter</span><br>
                    <p class="title-form-blog" style="">Receba nossos conteúdos em primeira mão. <strong>Assine nossa newsletter.</strong> </p>                    
                </div>

                <div class="with-content" id="" style="">

                    <form class="row contact_us_form" action="<?=site_url("Send/sendFormNewsletter")?>" method="POST"  id="form_fale_connosco" name="form_fale_connosco">
                        <div class="form-group col-md-12">
                            <input type="text" class="btn-form-blog" id="name_blog" name="name_blog" placeholder="Nome *" required>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="email" class="btn-form-blog" id="email_blog" name="email_blog" placeholder="E-mail *" required>
                        </div>                    

                        <!-- TRADUÇÃO   contact_botton -->
                        <div class="col-sm-4">
                            <button type="submit" value="submit" class="button-blog" id="btn_fale_con">Cadastrar </button>
                        </div> 
                                                
                    </form>
                    
                </div>


            </div>
              
            <div class="col-md-6 right-50 wow fadeInLeft- desktopOnly" style="transform: translate(-56px, -114px);">

                <div class="ads-img-cont" >
                    <img src="<?php base_url('img-blog-newsletter.png','img') ?>" alt="img" style="margin-top: 20%;  width: 125%; max-width: 225%;">
                </div>
                
            </div>

        </div>
                
    </div>

</div>
                
            