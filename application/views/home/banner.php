<!--================Slider Home Area =================-->
<!-- REVO SLIDER FULLSCREEN 1 -->
<?php
    
    $tablet  = false;
    $mobil   = false;
    $desktop = false;
    $data_y_tex_1_slide_1  = 0;
    $data_y_butom_slide_1  = 0;

    $data_y_butom_slide_2  = 0;

    $data_y_tex_1_slide_3  = 0;
    $data_y_butom_slide_3  = 0;

    $data_y_butom_slide_4  = 0;


    $position = 'left';


    if(isTablet()){
        $tablet  = true;
    }
    if(isMobile()){
        $mobil   = true;
        $position = 'center';
    }
    if($tablet == false && $mobil== false){
        $desktop = true;
    }

    if($tablet  == true){

        //echo "Olá, eu sou um tablet";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 495;

        $data_y_tex_1_slide_2  = 0;
        $data_y_butom_slide_2  = 460;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 495;

        $data_y_butom_slide_4  = 460;

    }else if($mobil  == true){

        //echo "Olá, eu sou um mobil";
        $data_y_tex_1_slide_1  = 510;
        $data_y_butom_slide_1  = 650;

        $data_y_butom_slide_2  = 600;

        $data_y_tex_1_slide_3  = 510;
        $data_y_butom_slide_3  = 650;

        $data_y_butom_slide_4  = 600;

    }elseif ($desktop == true) {
        
        //echo "Olá, eu sou um computador";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 480;

        $data_y_butom_slide_2  = 450;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 480;

        $data_y_butom_slide_4  = 450;
    }


?>

<main class="main">
      
      <div class="arrow-left hidden-xs"></div>
      <div class="arrow-right hidden-xs"></div>

      <!-- Start revolution slider -->

      <div class="rev_slider_wrapper">
        <div id="rev_slider" class="rev_slider fullscreenbanner">
          <ul>

            <!-- Slide 1 -->

            <li  data-transition="fade" data-masterspeed="1000" data-fsmasterspeed="1000" >

              <!-- Main image-->

            <?php

                if(isMobile()){

                    echo funGetAdvancedBanners('home_banner_topo', '
                    <!--<img src="{{img}}"  alt="slider"  data-bgposition="right top 30%" data-bgfit="cover" data-bgrepeat="no-repeat">-->
                    <img src="{{img}}"  alt="" data-bgposition="right 30% top"  data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="7">
                ');

                }else{

                    echo funGetAdvancedBanners('home_banner_topo', '
                        <!--<img src="{{img}}"  alt="slider"  data-bgposition="center top 30%" data-bgfit="cover" data-bgrepeat="no-repeat">-->
                        <img src="{{img}}"  alt="" data-bgposition="center center"  data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="7">
                    ');
                }

            ?>              

              

              <!-- Layer 1 -->

              <div class="tp-caption tp-shape tp-shape-mask tp-shapewrapper" 
                data-x="center" data-hoffset="" 
                data-y="left" data-voffset="" 
                data-basealign="slide" 
                data-width="['4000']"
                data-height="['4000']"
                data-transform_idle="o:1;"
                data-transform_in="y:[100%];z:0;opacity:0;rX:320deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
                data-transform_out="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power3.easeInOut;" 
                data-start="0" 
                data-responsive_offset="on" style="background-color:rgba(231,51,75,0.60); right:50%;"></div>

              <!-- Layer 2 -->

              <div class="slider-title tp-caption tp-resizeme" 
                data-x="['left']" data-hoffset="['0']"  
                data-y="['middle','middle','middle','middle']" 
                data-voffset="['-80']"
                data-fontsize="['90','70','60','50']"
                data-lineheight="['110','95','85']"
                data-height="none"
                data-whitespace="nowrap"
                data-transform_idle="o:1;"
                data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power2.easeInOut;" 
                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                data-mask_in="x:50px;y:0px;s:inherit;e:inherit;" 
                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                data-start="500" 
                data-splitin="chars" 
                data-splitout="none" 
                data-responsive_offset="on" 
                data-elementdelay="0.05" style="font-family: 'Montserrat', sans-serif; font-weight: 900;">Seu sorriso <br>merece o<br>melhor
              </div>

              <!-- Layer 2 -->

              <div class="tp-caption tp-resizeme OnlyDesktop" 
                data-x="['left']" data-hoffset="['0']"  
                data-y="['middle','middle','middle','middle']" 
                data-voffset="['150']"
                data-fontsize="['14']"
                data-height="none"
                data-whitespace="nowrap"
                data-transform_idle="o:1;"
                data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                data-mask_in="x:50px;y:0px;s:inherit;e:inherit;" 
                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                data-start="2000" 
                data-splitin="none" 
                data-splitout="none" 
                data-responsive_offset="on" 
                data-elementdelay="0.05"><div class="object-meta"> <h1 class="seo-banner">Atendimento de qualidade, com os <br> tratamentos mais modernos</h1> </div>
              </div>

              <!-- Layer 3 -->

              <div class="tp-caption tp-resizeme" 
                data-x="['left']" data-hoffset="['0']"  
                data-y="center" data-voffset="<?php echo isMobile() ? '230' : '230' ?>"
                data-voffset="['210']"
                data-fontsize="['14']"
                data-height="none"
                data-whitespace="nowrap"
                data-transform_idle="o:1;"
                data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                data-mask_in="x:50px;y:0px;s:inherit;e:inherit;" 
                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                data-start="2500" 
                data-splitin="none" 
                data-splitout="none" 
                data-responsive_offset="on" 
                data-elementdelay="0.05"><a href="#formulario" class="btn btn-cta-banner btn-amarelo smooth-scroll" data-toggle="modal-" style="box-shadow: none;">Marcar avaliação</a>
              </div>

              
          </li>

          <!-- Slide 2 -->

          <li  data-transition="incube" data-masterspeed="1000" data-fsmasterspeed="1000">

              <!-- Main image-->

              <?php

                if(isMobile()){

                    echo funGetAdvancedBanners('home_banner_topo_2', '
                    <!--<img src="{{img}}"  alt="slider"  data-bgposition="right top 30%" data-bgfit="cover" data-bgrepeat="no-repeat">-->
                    <img src="{{img}}"  alt="" data-bgposition="center center"  data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="7">
                ');

                }else{

                    echo funGetAdvancedBanners('home_banner_topo_2', '
                        <!--<img src="{{img}}"  alt="slider"  data-bgposition="center top 30%" data-bgfit="cover" data-bgrepeat="no-repeat">-->
                        <img src="{{img}}"  alt="" data-bgposition="center center"  data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="7">
                    ');
                }

            ?>     

              <!-- Layer 1 -->

              <div class="tp-caption tp-shape tp-shape-mask tp-shapewrapper" 
              id="slide-16-layer-2" 
              data-x="center" data-hoffset="" 
              data-y="left" data-voffset="" 
              data-basealign="slide" 
              data-width="['4000']"
              data-height="['4000']"
              data-transform_idle="o:1;"
              data-transform_in="x:[-100%];z:0;opacity:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
              data-transform_out="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power3.easeInOut;" 
              data-start="0" 
              data-responsive_offset="on" style="background-color:rgba(231,51,75,0.60);  right:50%;"></div>

              <!-- Layer 2 -->

              <div class="slider-title tp-caption tp-resizeme" 
                data-x="['left']" data-hoffset="['0']"  
                data-y="['middle','middle','middle','middle']" 
                data-voffset="['-80']"
                data-fontsize="['90','70','60','50']"
                data-lineheight="['110','95','85']"
                data-height="none"
                data-whitespace="nowrap"
                data-transform_idle="o:1;"
                data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power2.easeInOut;" 
                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                data-mask_in="x:50px;y:0px;s:inherit;e:inherit;" 
                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                data-start="500" 
                data-splitin="chars" 
                data-splitout="none" 
                data-responsive_offset="on" 
                data-elementdelay="0.05" style="font-family: 'Montserrat', sans-serif; font-weight: 900;">Como está <br>sua saúde<br>bucal?
              </div>

              <!-- Layer 2 -->

              <div class="tp-caption tp-resizeme OnlyDesktop" 
                data-x="['left']" data-hoffset="['0']"  
                data-y="['middle','middle','middle','middle']" 
                data-voffset="['150']"
                data-fontsize="['14']"
                data-height="none"
                data-whitespace="nowrap"
                data-transform_idle="o:1;"
                data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                data-mask_in="x:50px;y:0px;s:inherit;e:inherit;" 
                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                data-start="2000" 
                data-splitin="none" 
                data-splitout="none" 
                data-responsive_offset="on" 
                data-elementdelay="0.05"><div class="object-meta"> <h1 class="seo-banner"> Responda nosso Quiz e faça uma <br> avaliação sem compromisso.</h1></div>
              </div>

              <!-- Layer 3 -->

              <div class="tp-caption tp-resizeme" 
                data-x="['left']" data-hoffset="['0']"  
                data-y="center" data-voffset="<?php echo isMobile() ? '230' : '230' ?>"
                data-voffset="['210']"
                data-fontsize="['14']"
                data-height="none"
                data-whitespace="nowrap"
                data-transform_idle="o:1;"
                data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                data-mask_in="x:50px;y:0px;s:inherit;e:inherit;" 
                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                data-start="2500" 
                data-splitin="none" 
                data-splitout="none" 
                data-responsive_offset="on" 
                data-elementdelay="0.05"><a href="#formulario" class="btn btn-cta-banner btn-amarelo smooth-scroll" data-toggle="modal-" style="box-shadow: none;">Marcar avaliação</a>
              </div>

              
          </li>

          <!-- Slide 3 -->

          <li  data-transition="incube" data-masterspeed="1000" data-fsmasterspeed="1000">

              <!-- Main image-->

              <?php

                if(isMobile()){

                    echo funGetAdvancedBanners('home_banner_topo_3', '
                    <!--<img src="{{img}}"  alt="slider"  data-bgposition="right 30% top" data-bgfit="cover" data-bgrepeat="no-repeat">-->
                    <img src="{{img}}"  alt="" data-bgposition="right 30% top"  data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="7">
                ');

                }else{

                    echo funGetAdvancedBanners('home_banner_topo_3', '
                        <!--<img src="{{img}}"  alt="slider"  data-bgposition="center top 30%" data-bgfit="cover" data-bgrepeat="no-repeat">-->
                        <img src="{{img}}"  alt="" data-bgposition="center center"  data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="7">
                    ');
                }

            ?>     

              <!-- Layer 1 -->

              <div class="tp-caption tp-shape tp-shape-mask tp-shapewrapper" 
              id="slide-16-layer-2" 
              data-x="center" data-hoffset="" 
              data-y="left" data-voffset="" 
              data-basealign="slide" 
              data-width="['4000']"
              data-height="['4000']"
              data-transform_idle="o:1;"
              data-transform_in="x:[-100%];z:0;opacity:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
              data-transform_out="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power3.easeInOut;" 
              data-start="0" 
              data-responsive_offset="on" style="background-color:rgba(231,51,75,0.60);  right:50%;"></div>

              <!-- Layer 2 -->

              <div class="slider-title tp-caption tp-resizeme" 
                data-x="['left']" data-hoffset="['0']"  
                data-y="['middle','middle','middle','middle']" 
                data-voffset="['-80']"
                data-fontsize="['90','70','60','50']"
                data-lineheight="['110','95','85']"
                data-height="none"
                data-whitespace="nowrap"
                data-transform_idle="o:1;"
                data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power2.easeInOut;" 
                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                data-mask_in="x:50px;y:0px;s:inherit;e:inherit;" 
                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                data-start="500" 
                data-splitin="chars" 
                data-splitout="none" 
                data-responsive_offset="on" 
                data-elementdelay="0.05" style="font-family: 'Montserrat', sans-serif; font-weight: 900;">PRID é <br>Lessen <br>Dental<br>Clinics
              </div>

              <!-- Layer 2 -->

              <div class="tp-caption tp-resizeme OnlyDesktop" 
                data-x="['left']" data-hoffset="['0']"  
                data-y="['middle','middle','middle','middle']" 
                data-voffset="['150']"
                data-fontsize="['14']"
                data-height="none"
                data-whitespace="nowrap"
                data-transform_idle="o:1;"
                data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                data-mask_in="x:50px;y:0px;s:inherit;e:inherit;" 
                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                data-start="2000" 
                data-splitin="none" 
                data-splitout="none" 
                data-responsive_offset="on" 
                data-elementdelay="0.05"><div class="object-meta"> <h1 class="seo-banner"> Referência em Implantes Dentários </h1></div>
              </div>

              <!-- Layer 3 -->

              <div class="tp-caption tp-resizeme" 
                data-x="['left']" data-hoffset="['0']"  
                data-y="center" data-voffset="<?php echo isMobile() ? '230' : '230' ?>"
                data-voffset="['210']"
                data-fontsize="['14']"
                data-height="none"
                data-whitespace="nowrap"
                data-transform_idle="o:1;"
                data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                data-mask_in="x:50px;y:0px;s:inherit;e:inherit;" 
                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                data-start="2500" 
                data-splitin="none" 
                data-splitout="none" 
                data-responsive_offset="on" 
                data-elementdelay="0.05"><a href="#formulario" class="btn btn-cta-banner btn-amarelo smooth-scroll" data-toggle="modal-" style="box-shadow: none;">Marcar avaliação</a>
              </div>

              

          </li>


          </ul>
        </div>
      </div>

      <div class="mouse-helper">
        <span>Deslize para baixo</span>
        <a href="#sessao-1" class="smooth-scroll"><i class="icon ion-mouse"></i></a>
      </div>
    </main>