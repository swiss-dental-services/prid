
    <!--****************************************************** 3ra sessao  Nossas soluções******************************************************************-->        
<section class="events section bg-light">
        <div class="container">
          <div class="row">
            <header class="text-center col-md-8 col-md-offset-2">
              <h2 class="section-title"><?=home_three_text_2?></h2>
            </header>
          </div>
        </div>
        <div class="section-content-2">
          <div class="container">
            <div class="row-base row">
              <div class="col-event col-base col-md-4 wow fadeInLeft">
                <a href="tratamentos#ROTA">
                <div class="event bg-violet">
                  <div class="bg-content-1">
                    <h4 class="event-title">ROTA® <br> REABILITAÇÃO<br> ORAL TOTAL<br>AVANÇADA</h4>
                    <div class="event-date">SOLUÇÃO COMPLETA</div>
                  </div>
                </div>
                </a>
              </div>
              <div class="col-event col-base col-md-4 wow fadeInLeft" data-wow-delay="0.3s">
                <a href="tratamentos#ID">
                  <div class="event bg-dark-blue">
                    <div class="bg-content-2">
                      <h4 class="event-title">IMPLANTES <br>UNITÁRIOS</h4>
                      <div class="event-date">SOLUÇÃO PERSONALIZADA</div>
                    </div>
                  </div>
                </a>
              </div>
              <div class="col-event col-base col-md-4 wow fadeInLeft" data-wow-delay="0.6s">
                <a href="tratamentos#TID">
                  <div class="event bg-yellow">
                    <div class="bg-content-3">
                      <h4 class="event-title">TID® <br> TRATAMENTO<br>DE IMPLANTES<br>DENTÁRIOS</h4>
                      <div class="event-date">SOLUÇÃO PRÁTICA</div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>