
   <!--****************************************************** 6ta sessao Depoimentos**************************************************************************--> 

<section class="clients bg-light section">
  <div class="container">
      <div class="row">
        <header class="text-center col-md-8 col-md-offset-2">
        <h2 class="section-title"><?=home_six_text_1?></h2>
        <!--<p class="section-lead">Our team of experts has competences of different<br> branches and specializations.</p> -->
        </header>
      </div>
    </div>
    <div class="section-content">
      <div class="container-fluid">
        <div class="col-md-6 col-md-offset-3">
          <div class="clients-wrapper">
            <div class="client-carousel js-client-carousel">
            <?php
                echo funGetSlide('home_six','','','

                 <div class="client">
                    <span class="quote">"</span>
                    <div style="display: flex; justify-content: center;" class="OnlyDesktop">
                      <img src="{{img}}" style="width: 50%; margin-bottom: 30px;">
                    </div>
                    <p class="client-text">{{text}}</p>
                    <span class="h5 client-name">{{title}}   —   </span>
                    <i>{{subtitle}}</i>
                  </div>
                
                ');
            ?>              

              <!--<div class="client">
                <span class="quote">"</span>
                <div style="display: flex; justify-content: center;" class="OnlyDesktop">
                  <img src="<?php /*base_url('depoimentos-antonio.png','gallery/images')*/ ?>" style="width: 50%; margin-bottom: 30px;">
                </div>
                <p class="client-text">
                   “Usei próteses móveis antes de conhecer a clínica, mas nunca me adaptei a elas. Tinha muita vergonha de comer em público e medo da prótese cair. Depois que coloquei o Implante Dentário, que é definitivo, minha vida mudou completamente!”</p>
                <span class="h5 client-name">António Moutinho   —   </span>
                <i>Tratamento: Próteses Dentárias</i>
              </div>
              <div class="client">
                <span class="quote">"</span>
                <div style="display: flex; justify-content: center;" class="OnlyDesktop">
                  <img src="<?php /*base_url('depoimentos-ricardo.png','gallery/images')*/ ?>" style="width: 50%; margin-bottom: 30px;">
                </div>
                <p class="client-text">
                  “Ter colocado Implantes Dentários é totalmente diferente porque agora já posso sorrir e mastigar melhor, e já consigo falar com as pessoas de boca aberta, com um sorriso. Antigamente não era assim, mas agora já posso sorrir! Gostei muito do resultado!"</p>
                <span class="h5 client-name">Ricardo Gonçalves   —   </span>
                <i>Tratamento: Próteses Dentárias</i>
              </div>
              <div class="client">
                <span class="quote">"</span>
                <div style="display: flex; justify-content: center;" class="OnlyDesktop">
                  <img src="<?php /*base_url('depoimentos-fatima.png','gallery/images')*/ ?>" style="width: 50%; margin-bottom: 30px;">
                </div>
                <p class="client-text">
                  “Usava as próteses removíveis, mas era muito cansativo e eu já estava um pouco saturada! Não me sentia bem. Depois que começaram a falar dos implantes, que havia possibilidades, até me davam uma certa facilidade de pagamentos, eu comecei a tratar disso." </p>
                <span class="h5 client-name">Fátima Moraes   —   </span>
                <i>Tratamento: coroas</i>
              </div>-->
              
            </div>
          </div>
        </div>
      </div>
    </div>
</section>