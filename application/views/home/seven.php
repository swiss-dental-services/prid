
    <!--****************************************************** 7ma sessao  Depoimentos****************************************************************************-->

<section class="contacts-3">
        <section class="clearfix">
          <div class="col-md-4 address-item bg-primary-" style="background: #d70f2a url(assets/img/custon/icon_phone.png) 50% 0 no-repeat; background-size: contain;">
            <div class="address-icon-" style="margin-top: 60px;">
              <i class="icon ion-iphone-"></i>
            </div>
            <div class="address-title" style="transform: translateY(15px); color: #fff;">(48) 98803 8278 | (48) 3181 0188 <br> +8 800 555 35 35 </div>
          </div>
          <div class="col-md-4 address-item bg-light-" style="background: #bcbaba;">
            <div class="address-icon">
              <p style="font-weight: bold; font-size: 14px;">UNIDADES</p>
            </div>
            <div class="address-title" style="transform: translateY(15px); color: #fff;">Florianópilis-SC <br> Porto Alegre-RS</div>
          </div>
          <div class="col-md-4 address-item bg-dark-" style="background: #ea4f5d url(assets/img/custon/icon_loc.png) 50% 0 no-repeat; background-size: contain;">
            <div class="address-icon" style="border: none;">
              <i class="icon ion-ios-location-outline-"></i>
            </div>
            <div class="address-title" style="transform: translateY(15px); color: #fff;"> <b>Unidade I:</b> R. Dom Jaime Câmara, Nº 170 Sala 701 - Centro / <b>Unidade II:</b> R. Vidal Ramos, 53 Sala 504 - Ed Cristal Center - Centro CEP 88010-320 <br> Av. Osvaldo Aranha, Nº 1022 Sala 1506 - Bom Fim
            </div>
          </div>
        </section>
        
      </section>