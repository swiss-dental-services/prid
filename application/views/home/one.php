<!--****************************************************** 1ra sessao  **********************************************************************-->
        
<section class="features bg-dark- section" id="sessao-1">

  <div class="container">
    <div class="row">
      <header class="text-center col-md-8 col-md-offset-2">
        <h2 class="section-title"><?=home_three_text_1?></h2>
        <!--<p class="section-lead"><?/*=home_three_text_2*/?><br></p>-->
      </header>
    </div>
  </div>

  <div class="container">
    <div class="row-base row">
      <?php
        echo funGetSlide('home_three','','','

          <div class="col-base col-feature col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="{{callAction}}">
            <img class="img-solucoes" src="{{img}}">
            <h4 class="text-white-">{{title}}</h4>
            <p class="bloco">{{text}}</p>
          </div>
          
        ');
      ?>
            
    </div>
  </div>

  <div class="container">
    <div class="row">
      <header class="text-center col-md-8 col-md-offset-2 mt-50">
        <a href="#formulario" data-toggle="modal-" class="btn btn-violet- btn-red wow swing smooth-scroll" style="visibility: visible; animation-name: swing;">Marcar avaliação</a>
      </header>
    </div>
  </div>
</section>



    