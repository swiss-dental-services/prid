<!--****************************************************** contact  *****************************************************************************-->
<!--<section id="formulario" class="section section-button- bg-form">
    
    <div class="page-section">
        <div class="container">
            <div class="row">
                
                <div class="col-md-6 right-50 wow fadeInLeft- OnlyDesktop" style="position: relative;">
                            
                    <div class="ads-img-cont" >
                        <img src="<?php /*base_url('img_formulario.png','img/custon')*/ ?>" alt="img" style="margin-top: 0%; max-width: 225%; position: absolute; transform: translate(-91px, -58px);">
                    </div>
    
                </div>


                <div class="col-md-6 left-50 right-desktop" style="display: flex; justify-content: center; align-items: center; flex-direction: column;">
                    <div class="title-fs-45 uppercase mt-50 mb-50"></div>

                    <div class="with-content" id="form-home-TSS"></div>


                </div>
  
  
            </div>
        </div>
    </div>    
    
</section> -->

<section class="section bg-form" id="formulario">
  <div class="container">
    <div class="row" style="margin-bottom: 50px;">
        <div class="col-md-12 text-center">            
            <h2 class="title-form-mob" style="color: #fff; font-weight: normal;">Conquiste o sorriso que sempre quis. <br> Fale conosco.</h2>
        </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <form id="FORM-PRID" method="post">
          <div class="col-sm-4 form-group">
            <input type="text"  name="name" id="name" placeholder="Nome *" required>
          </div>
          <div class="col-sm-4 form-group">
            <input type="email" name="email" id="email" required="" placeholder="Email *" required>
          </div>
          <div class="col-sm-4 form-group">
            <input type="text" name="phone" id="phone" required="" placeholder="Telefone *" required>
          </div>
          <div class="col-sm-4 form-group">
              <select name="q1" id="q1" required>
                  <option value="">Clinica mais próxima</option>
                  <option value="Florianópolis">Florianópolis</option>
                  <option value="Curitiba">Curitiba</option>                  
              </select>
          </div>
          <div class="col-sm-4 form-group">
              <select name="melhor_altura_para_contacto" id="melhor_altura_para_contacto" required>
                  <option value="">Melhor hora para falar</option>
                  <option value="manha">Manhã</option>
                  <option value="tarde">Tarde</option>                  
              </select>
          </div>
          <div class="col-sm-12">
              <input type="checkbox" id="termos" name="termos" value="" required>
              <label> <a href="termos-condicoes" target="_blank" style="color: #fff; font-weight: normal;">Li e aceito os termos e condições</a></label> 
          </div>
          <div class="col-sm-12 text-right">
            <button type="submit" class="btn btn-white" id="btn-1">Enviar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

