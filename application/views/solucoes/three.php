
    <!--****************************************************** 3ra sessao  Rota® - Reabilitação Oral Total Avançada***********************************************-->        

<section class="features bg-content-quiz section-banner-2 section">

  <div class="container">    

    <div class="row pt-150">
      <header class="text-center col-md-8 col-md-offset-2" style="margin-bottom: 55px;">
        <h1 class="section-title section-title-mobil" style="color: #fff"><?=solucoes_three_text_1?></h1>              
      </header>
    </div>                           
     
  </div>

  <div class="container">
    <div class="row-base row">
            
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2 wow fadeInUp">
        <div class="banner-in-section banner banner-blue">
          <div class="banner-content">
            <h2 class="banner-title"> <?=solucoes_three_text_2?> <br> <span style="font-weight: normal;"><?=solucoes_three_text_3?></span></h2>
            <a href="quiz" data-toggle="modal-" class="btn btn-white wow swing smooth-scroll title-m">Participar do Quiz</a>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>