<!--****************************************************** 1ra sessao  **********************************************************************-->
        
<section class="porftolio section" id="sessao-1">
    <div class="container">
      <div class="row">
        <header class="text-center col-md-8 col-md-offset-2">
        <h2 class="section-title"><?=solucoes_one_text_1?></h2>
        <p class="section-lead"><?=solucoes_one_text_2?></p>
        </header>
      </div>
      <div class="isotope isotope-space- js-gallery" style="margin-top: 62px;">

        <?php
            echo funGetSlide('solucoes_one','','','

             <div class="isotope-item w50 wow fadeInUp digital col-md-4- card-solucoes" data-wow-delay="{{ctaTitle}}">
                <a href="{{img}}" title="{{title}}">
                  <figure class="showcase-item">
                    <div class="showcase-item-thumbnail"><img alt="" src="{{img}}"></div>
                    <figcaption class="showcase-item-hover hover-blue">
                      <div class="showcase-item-info">
                        <div class="showcase-item-category">{{title}}</div>
                        <div class="showcase-item-title- text-custon-so">{{text}}</div>
                        <i class="ion-ios-plus-empty"></i>
                      </div>
                    </figcaption>
                  </figure>
                </a>
              </div>
            
            ');
        ?>

               
      </div>
      <!--<div class="text-center section-content"><a href="" class="btn btn-violet">See all projects</a>-->
    </div>
    <div class="container">
      <div class="row">
        <header class="text-center col-md-8- col-md-offset-2- mt-50- section-content" style="display: flex;justify-content: center;">
          <a href="tratamentos" data-toggle="modal-" class="btn btn-violet- btn-red wow swing smooth-scroll" style="visibility: visible; animation-name: swing;">Saiba mais</a>
        </header>
      </div>
    </div>  
  </section>


    