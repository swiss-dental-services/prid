<!--================Slider Home Area =================-->
<!-- REVO SLIDER FULLSCREEN 1 -->
<?php
    
    $tablet  = false;
    $mobil   = false;
    $desktop = false;
    $data_y_tex_1_slide_1  = 0;
    $data_y_butom_slide_1  = 0;

    $data_y_butom_slide_2  = 0;

    $data_y_tex_1_slide_3  = 0;
    $data_y_butom_slide_3  = 0;

    $data_y_butom_slide_4  = 0;


    $position = 'left';


    if(isTablet()){
        $tablet  = true;
    }
    if(isMobile()){
        $mobil   = true;
        $position = 'center';
    }
    if($tablet == false && $mobil== false){
        $desktop = true;
    }

    if($tablet  == true){

        //echo "Olá, eu sou um tablet";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 495;

        $data_y_tex_1_slide_2  = 0;
        $data_y_butom_slide_2  = 460;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 495;

        $data_y_butom_slide_4  = 460;

    }else if($mobil  == true){

        //echo "Olá, eu sou um mobil";
        $data_y_tex_1_slide_1  = 510;
        $data_y_butom_slide_1  = 650;

        $data_y_butom_slide_2  = 600;

        $data_y_tex_1_slide_3  = 510;
        $data_y_butom_slide_3  = 650;

        $data_y_butom_slide_4  = 600;

    }elseif ($desktop == true) {
        
        //echo "Olá, eu sou um computador";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 480;

        $data_y_butom_slide_2  = 450;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 480;

        $data_y_butom_slide_4  = 450;
    }


?>
<!-- Home -->

<?php

  if(isMobile()){

    echo funGetAdvancedBanners('solucoes_banner_topo', '

      <main class="main main-full main-startup" data-stellar-background-ratio="0.7" style="background: url({{img}}) center center no-repeat;">
        <div class="container">
          <div class="opener">
            <div class="text-center">
              <h1>{{title}} <br> {{subtitle}}</h1>
              <div class="lead-hr wow fadeInLeft"></div>
              <p class="lead wow fadeInUp" data-wow-delay="0.3s">{{text}}</p>
              <a href="https://www.youtube.com/watch?v=1zr1EMONLvU" class="icon-home-video v2 js-video-play wow fadeInRight" data-wow-delay="0.6s"><i class="ion-ios-play"></i></a>
            </div>
          </div>
        </div>
        <div class="mouse-helper">
          <span>Deslize para baixo</span>
          <a href="#sessao-1" class="smooth-scroll"><i class="icon ion-mouse"></i></a>
        </div>
      </main>
      
      ');

  }else{

    echo funGetAdvancedBanners('solucoes_banner_topo', '

      <main class="main main-full main-startup" data-stellar-background-ratio="0.7" style="background: url({{img}}) center center no-repeat;">
        <div class="container">
          <div class="opener">
            <div class="text-center">
              <h1>{{title}} <br> {{subtitle}}</h1>
              <div class="lead-hr wow fadeInLeft"></div>
              <p class="lead wow fadeInUp" data-wow-delay="0.3s">{{text}}</p>
              <a href="https://www.youtube.com/watch?v=1zr1EMONLvU" class="icon-home-video v2 js-video-play wow fadeInRight" data-wow-delay="0.6s"><i class="ion-ios-play"></i></a>
            </div>
          </div>
        </div>
        <div class="mouse-helper">
          <span>Deslize para baixo</span>
          <a href="#sessao-1" class="smooth-scroll"><i class="icon ion-mouse"></i></a>
        </div>
      </main>
      
      ');
  }

?>