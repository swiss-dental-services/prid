<!--****************************************************** 2da sessao  *************************************************************-->

<section class="mission-2 section" id="ROTA">
  <div class="bg-mission-2-custon bg-left-fluid col-md-6 hidden-sm hidden-xs"></div>

  <div class="right-fluid-content col-md-6 col-lg-4 col-md-offset-6">
    <?php
      echo funGetAdvancedBanners('solucoes_three', '

        <header class="text-center">
          <h2 class="text-left section-title-3">{{title}}</h2>
        </header>
        <div class="about-entry">
          <p>{{text}}</p>
          <p>{{subtext}}</p>
        </div>

        <div class="row">
          <header class="text-center col-md-8 col-md-offset-2- mt-50" style="display: flex;">
            <a href="{{callAction}}" data-toggle="modal-" class="btn btn-violet- btn-red wow swing smooth-scroll" style="visibility: visible; animation-name: swing;">{{callTitle}}</a>
          </header>
        </div>      

      ');
    ?>
  </div>
  
</section>