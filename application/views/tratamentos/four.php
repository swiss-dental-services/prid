   <!--****************************************************** 4ta sessao **********************************************************************************--> 

<section class="mission-2 section bg-vermelho">
    <div class="bg-mission-4-custon bg-left-fluid col-md-6 hidden-sm hidden-xs"></div>
    <div class="right-fluid-content col-md-6 col-lg-4 col-md-offset-6">
      <header class="text-center">
        <h2 class="text-left text-white section-title-2"><?=tratamentos_four_text_1?></h2>
      </header>

      <!-- Services -->

      <div class="row-service row-base row">
      	<?php
	       echo funGetSlide('tratamentos_four','','','

	       	<div class="col-base col-service col-feature col-sm-6 col-md-6 wow fadeInUp" data-wow-delay="{{ctaTitle}}">
	          <img class="img-solucoes" src="{{img}}">
	          <h4 class="text-white">{{title}}</h4>
	          <p class="bloco">{{text}}</p>
	        </div>
	        
	        ');
	    ?>        
        <div class="clearfix visible-sm visible-md"></div>

      </div>
    </div>
</section>