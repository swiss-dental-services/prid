
    <!--****************************************************** 7ma sessao  ****************************************************************************-->

<section class="clients section" style="padding-top: 0.0em;">
  <div class="container">
      <div class="row">
        <header class="text-center col-md-8 col-md-offset-2">
        <h2 class="section-title"><?=tratamentos_seven_text_1?></h2>
        <p class="section-lead"><?=tratamentos_seven_text_2?><br></p>
        </header>
      </div>
    </div>
    <div class="section-content" style="margin-top: 3.8em;">
      <div class="container">
        <div class="col-md-12 col-md-offset-3-">
           
          <!-- ACORDION-->
              <div class="accordion center-div" id="accordionExample" style="">

                  
                  <div class="card wow animated fadeInLeft"  data-wow-delay="0.7s">

                    <div class="card-header" id="heading1">
                      <h5 class="mb-0">
                        <button class="btn-acordion btn-link mobil-btn" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1" style="color: gray;">
                            <?/*=feito_para_voce_faq_1*/?> O que é PRID?                             
                        </button>
                        <img src="assets/img/custon/caret-down-outline.svg" class="img-acordion">
                      </h5>
                    </div>

                      <div id="collapse1" class="collapse show-" aria-labelledby="heading1" data-parent="#accordionExample">
                        <div class="card-body">
                          <?/*=feito_para_voce_faq_2*/?>Somos o Programa Regional de Implantes Dentários, focado em saúde, bem-estar e autoestima, com tratamentos odontológicos voltados para a Reabilitação Oral.
                        </div>
                      </div>                 

                  </div>


                  <div class="card wow animated fadeInRight"  data-wow-delay="0.9s">

                    <div class="card-header" id="heading2">
                      <h5 class="mb-0">
                        <button class="btn-acordion btn-link mobil-btn" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2" style="color: gray;">
                            <?/*=feito_para_voce_faq_3*/?>Qual o propósito da PRID?                              
                        </button>
                        <img src="assets/img/custon/caret-down-outline.svg" class="img-acordion">
                      </h5>
                    </div>

                      <div id="collapse2" class="collapse show-" aria-labelledby="heading2" data-parent="#accordionExample">
                        <div class="card-body">
                          <?/*=feito_para_voce_faq_4*/?> Melhorar as vidas dos nossos pacientes, resgatar sua qualidade de vida, autoestima e confiança, através da colocação de Implantes Dentários, com atendimentos e serviços de qualidade.
                        </div>
                      </div>                 

                  </div>


                  <div class="card wow animated fadeInLeft"  data-wow-delay="1.1s">

                    <div class="card-header" id="heading3">
                      <h5 class="mb-0">
                        <button class="btn-acordion btn-link mobil-btn" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3" style="color: gray;">
                           <?/*=feito_para_voce_faq_5*/?>Quanto custa os tratamentos?                              
                        </button>
                        <img src="assets/img/custon/caret-down-outline.svg" class="img-acordion">
                      </h5>
                    </div>

                      <div id="collapse3" class="collapse show-" aria-labelledby="heading3" data-parent="#accordionExample">
                        <div class="card-body">
                          <?/*=feito_para_voce_faq_6*/?>Ao comparecer na consulta de avaliação, realizamos um diagnóstico com a melhor solução para seu problema. Depois disso, o orçamento é adaptado às suas necessidades, seja financeira ou de serviço. Vale ressaltar que existe possibilidade de parcelamento e condições diferenciadas apresentadas após avaliação e em casos individuais.
                        </div>
                      </div>                 

                  </div>


                  <div class="card wow animated fadeInRight"  data-wow-delay="1.3s">

                    <div class="card-header" id="heading4">
                      <h5 class="mb-0">
                        <button class="btn-acordion btn-link mobil-btn" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4" style="color: gray;">
                            <?/*=feito_para_voce_faq_7*/?>A consulta de avaliação terá algum custo?                      
                        </button>
                        <img src="assets/img/custon/caret-down-outline.svg" class="img-acordion">
                      </h5>
                    </div>

                      <div id="collapse4" class="collapse show-" aria-labelledby="heading4" data-parent="#accordionExample">
                        <div class="card-body">
                          <?/*=feito_para_voce_faq_8*/?>Não. Estaremos sempre disponíveis para esclarecer qualquer questão ou dúvida que você tenha e iremos aconselhá-lo sobre qual tratamento que deverá ser realizado. Marque já uma consulta sem compromisso.
                        </div>
                      </div>                 

                  </div>


                  <div class="card wow animated fadeInLeft"  data-wow-delay="1.5s">

                    <div class="card-header" id="heading5">
                      <h5 class="mb-0">
                        <button class="btn-acordion btn-link mobil-btn" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5" style="color: gray;">
                            <?/*=feito_para_voce_faq_1*/?> Quais são as formas de pagamentos aceitas nas clínicas?                            
                        </button>
                        <img src="assets/img/custon/caret-down-outline.svg" class="img-acordion">
                      </h5>
                    </div>

                      <div id="collapse5" class="collapse show-" aria-labelledby="heading5" data-parent="#accordionExample">
                        <div class="card-body">
                          <?/*=feito_para_voce_faq_2*/?>Aceitamos o pagamento nas mais variadas formas (dinheiro, cartões de crédito, além de nosso cartão pré-pago).

                        </div>
                      </div>                 

                  </div>


                  <div class="card wow animated fadeInRight"  data-wow-delay="1.7s">

                    <div class="card-header" id="heading6">
                      <h5 class="mb-0">
                        <button class="btn-acordion btn-link mobil-btn" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6" style="color: gray;">
                            <?/*=feito_para_voce_faq_3*/?>A cirurgia para colocação de Implantes Dentários é dolorosa?                              
                        </button>
                        <img src="assets/img/custon/caret-down-outline.svg" class="img-acordion">
                      </h5>
                    </div>

                      <div id="collapse6" class="collapse show-" aria-labelledby="heading6" data-parent="#accordionExample">
                        <div class="card-body">
                          <?/*=feito_para_voce_faq_4*/?> A cirurgia é feita sob anestesia, sem qualquer dor ou desconforto para o paciente. Após a intervenção são recomendados analgésicos e compressas geladas até ao dia seguinte, para diminuir o edema.
                        </div>
                      </div>                 

                  </div>


                  <div class="card wow animated fadeInLeft"  data-wow-delay="1.9s">

                    <div class="card-header" id="heading7">
                      <h5 class="mb-0">
                        <button class="btn-acordion btn-link mobil-btn" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7" style="color: gray;">
                           <?/*=feito_para_voce_faq_5*/?>Após colocar Implantes Dentários que tipo de alimentação devo ter?                             
                        </button>
                        <img src="assets/img/custon/caret-down-outline.svg" class="img-acordion">
                      </h5>
                    </div>

                      <div id="collapse7" class="collapse show-" aria-labelledby="heading7" data-parent="#accordionExample">
                        <div class="card-body">
                          <?/*=feito_para_voce_faq_6*/?>Mantenha uma alimentação líquida e fria durante o primeiro dia após a cirurgia. Prefira iogurtes, vitaminas e alimentos gelados. Comidas e bebidas muito quentes, ácidos ou alimentos mais duros devem ser evitados nos primeiros dias após a cirurgia.
                        </div>
                      </div>                 

                  </div>


                  <div class="card wow animated fadeInRight"  data-wow-delay="2.1s">

                    <div class="card-header" id="heading8">
                      <h5 class="mb-0">
                        <button class="btn-acordion btn-link mobil-btn" type="button" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8" style="color: gray;">
                            <?/*=feito_para_voce_faq_7*/?>Existe rejeição dos Implantes Dentários?                             
                        </button>
                        <img src="assets/img/custon/caret-down-outline.svg" class="img-acordion">
                      </h5>
                    </div>

                      <div id="collapse8" class="collapse show-" aria-labelledby="heading8" data-parent="#accordionExample">
                        <div class="card-body">
                          <?/*=feito_para_voce_faq_8*/?>Não ocorre rejeição. Uma vez que o titânio é um material que não apresenta na sua composição qualquer elemento que interfira com o processo de osteointegração. Contudo, o sucesso da colocação de Implantes Dentários vai depender sempre de diversos fatores: Planejamento cirúrgico; Período de cicatrização; Tempo de recuperação do paciente

                        </div>
                      </div>                 

                  </div>

                  
              </div>
              <!-- FIM DE ACORDION-->

        </div>
      </div>
    </div>
</section>