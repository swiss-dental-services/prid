<!DOCTYPE html>
    <html lang="pt-br">
			<!-- Start Headers Includes ===========
			======================================= -->
			<?php
	            require_once 'includes/generateAllInfo.php';
	            
				include('helper.php');
				
				include('header.php');
				
			?>
     	<body class="bg-page">

     		<!-- LOADER -->	
			<div class="loader">
			  <div class="spinner">
			    <div class="double-bounce1"></div>
			    <div class="double-bounce2"></div>
			  </div>
			</div>

			<?php 
			    include('nav/bar-nav.php');
			     			
			    include('tratamentos/banner.php'); 
			?>

		    <div class="content">
	                
				<!-- ==================================
				================== End Headers Includes -->
				<!-- Start Page Struture ==============
				======================================= -->
				<?php include('tratamentos/structure.php'); ?>
				<!-- ==================================
				===================== End Page Struture -->	
				<!-- Start Footers Includes ===========
				======================================= -->
				<?php	include('footer.php');	?>
				
			</div>
			<?php	include('commum-js.php');	?>
				<!-- ==================================
				================== End Footers Includes -->

	    </body>
    </html>
    <script type="text/javascript">
      
      /*-------------------------progrmamcion acordion-----------------------------------*/
      var count_heading1  = 0;
      var count_heading2  = 0;
      var count_heading3  = 0;
      var count_heading4  = 0;
      var count_heading5  = 0;
      var count_heading6  = 0;
      var count_heading7  = 0;
      var count_heading8  = 0;

      $("#heading1").click(function(){
          
          if(count_heading1==0){

              $("#collapse1").slideDown("slow");
              count_heading1++;
              
            }else{
              $("#collapse1").slideUp("slow");
              count_heading1--;
          }
          
          //fechando todas los div menos el activo
          $(".collapse").each(function(){              
              
              if($(this).attr("id")!= "collapse1"){

                $(this).slideUp("slow");

              }
          });

        });

        $("#heading2").click(function(){
          
          if(count_heading2==0){

              $("#collapse2").slideDown("slow");
              count_heading2++;
              
            }else{
              $("#collapse2").slideUp("slow");
              count_heading2--;
          }

          $(".collapse").each(function(){
              
              if($(this).attr("id")!= "collapse2"){

                $(this).slideUp("slow");
                
              }
          });

        });

        $("#heading3").click(function(){
          
          if(count_heading3==0){

              $("#collapse3").slideDown("slow");
              count_heading3++;
              
            }else{
              $("#collapse3").slideUp("slow");
              count_heading3--;
          }

          //fechando todas los div menos el activo
          $(".collapse").each(function(){              
              
              if($(this).attr("id")!= "collapse3"){

                $(this).slideUp("slow");

              }
          });

        });

        $("#heading4").click(function(){
          
          if(count_heading4==0){

              $("#collapse4").slideDown("slow");
              count_heading4++;
              
            }else{
              $("#collapse4").slideUp("slow");
              count_heading4--;
          }

          //fechando todas los div menos el activo
          $(".collapse").each(function(){              
              
              if($(this).attr("id")!= "collapse4"){

                $(this).slideUp("slow");

              }
          });

        });

        $("#heading5").click(function(){
          
          if(count_heading5==0){

              $("#collapse5").slideDown("slow");
              count_heading5++;
              
            }else{
              $("#collapse5").slideUp("slow");
              count_heading5--;
          }

          //fechando todas los div menos el activo
          $(".collapse").each(function(){              
              
              if($(this).attr("id")!= "collapse5"){

                $(this).slideUp("slow");

              }
          });

        });

        $("#heading6").click(function(){
          
          if(count_heading6==0){

              $("#collapse6").slideDown("slow");
              count_heading6++;
              
            }else{
              $("#collapse6").slideUp("slow");
              count_heading6--;
          }

          //fechando todas los div menos el activo
          $(".collapse").each(function(){              
              
              if($(this).attr("id")!= "collapse6"){

                $(this).slideUp("slow");

              }
          });

        });

        $("#heading7").click(function(){
          
          if(count_heading7==0){

              $("#collapse7").slideDown("slow");
              count_heading7++;
              
            }else{
              $("#collapse7").slideUp("slow");
              count_heading7--;
          }

          //fechando todas los div menos el activo
          $(".collapse").each(function(){              
              
              if($(this).attr("id")!= "collapse7"){

                $(this).slideUp("slow");

              }
          });

        });

        $("#heading8").click(function(){
          
          if(count_heading8==0){

              $("#collapse8").slideDown("slow");
              count_heading8++;
              
            }else{
              $("#collapse8").slideUp("slow");
              count_heading8--;
          }

          //fechando todas los div menos el activo
          $(".collapse").each(function(){              
              
              if($(this).attr("id")!= "collapse8"){

                $(this).slideUp("slow");

              }
          });

        });

        

    </script>