
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

                
        <link rel="apple-touch-icon" sizes="180x180" href="<?php base_url('apple-touch-icon.png','img/favicon') ?>">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php base_url('favicon-32x32.png','img/favicon') ?>">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php base_url('favicon-16x16.png','img/favicon') ?>">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags 
        <title>PRID </title>-->

        <?php 
            $URL_ATUAL= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 

            $URL_ATUAL = explode("/", $URL_ATUAL);
            
            if (in_array("solucoes-oferecidas", $URL_ATUAL)) { ?>

                <META NAME="DESCRIPTION" CONTENT="Soluções personalizadas em próteses e implantes dentários. Atendimento de excelência e condições de pagamento que se adequam ao seu bolso.">
                <META NAME="ABSTRACT" CONTENT="Apresentar as soluções e opções de pagamento">
                <META NAME="KEYWORDS" CONTENT="PRID, Lessen Dental Clinic, implantes, implantes dentários, sorriso, dentes, higiene bucal, saúde bucal, reabilitação oral total avançada, implantes unitários, TID, prótese, prótese dentária, prótese fixa, prótese removível, dente, coroa, titânio, porcelana, dentição, odontologia, clínica odontológica, clínica dentária, tratamento odontológico, dentista, tratamento dentário." >
                <title> Soluções eficazes para sua saúde bucal | PRID </title>

        <?php    }elseif (in_array("quem-somos", $URL_ATUAL)) { ?>

                <META NAME="DESCRIPTION" CONTENT="Conheça o PRID, um movimento da Lessen Dental Clinic, rede de clínicas odontológicas com profissionais experientes e tecnologia de ponta.">
                <META NAME="ABSTRACT" CONTENT="Apresentar a marca e os objetivos">
                <META NAME="KEYWORDS" CONTENT="PRID, Lessen Dental Clinic, implantes, implantes dentários, sorriso, dentes, higiene bucal, saúde bucal, reabilitação oral total avançada, implantes unitários, TID, prótese, prótese dentária, dentição, odontologia, clínica odontológica, clínica dentária, tratamento odontológico, dentista, tratamento dentário." >
                <title> Quem Somos | PRID </title>

        <?php    }elseif (in_array("blog", $URL_ATUAL)) { ?>

                <META NAME="DESCRIPTION" CONTENT="Tire suas dúvidas sobre saúde bucal, tratamentos odontológicos e muito mais. No blog do PRID, você aprende com quem é referência em próteses e implantes dentários.">
                <META NAME="ABSTRACT" CONTENT="Apresentar conteúdos educativos sobre o tema do portal">
                <META NAME="KEYWORDS" CONTENT="PRID, Lessen Dental Clinic, implantes, implantes dentários, sorriso, dentes, higiene bucal, saúde bucal, reabilitação oral total avançada, implantes unitários, TID, prótese, prótese dentária, recuperação oral, prótese fixa, prótese removível, dente, coroa, titânio, porcelana, dentição, odontologia, clínica odontológica, clínica dentária, tratamento odontológico, dentista, tratamento dentário." >
                <title> Blog do PRID | Saiba mais sobre saúde bucal </title>

        <?php    }elseif (in_array("tratamentos", $URL_ATUAL)) { ?>

                <META NAME="DESCRIPTION" CONTENT="Descubra qual é o melhor tratamento para resgatar seu sorriso. Entenda os tipos de implante disponíveis e tire suas dúvidas!">
                <META NAME="ABSTRACT" CONTENT="Apresentar os tratamentos em maior detalhe, tirando dúvidas comuns.">
                <META NAME="KEYWORDS" CONTENT="PRID, Lessen Dental Clinic, implantes, implantes dentários, sorriso, dentes, higiene bucal, saúde bucal, reabilitação oral total avançada, implantes unitários, TID, prótese, prótese dentária, recuperação oral, prótese fixa, prótese removível, dente, coroa, titânio, porcelana, dentição, odontologia, clínica odontológica, clínica dentária, tratamento odontológico, dentista, tratamento dentário, tipos de implante dentário, tipos de prótese dentária" >
                <title> Tratamentos | Conheça nossas opções de próteses e implantes dentários </title>

        <?php    }elseif (in_array("clinicas", $URL_ATUAL)) { ?>

                <META NAME="DESCRIPTION" CONTENT="Conheça a estrutura das clínicas PRID em Florianópolis e Curitiba. Equipamentos de ponta e profissionais experientes para garantir seu sorriso!">
                <META NAME="ABSTRACT" CONTENT="Apresentar as duas unidades do PRID">
                <META NAME="KEYWORDS" CONTENT="PRID, Lessen Dental Clinic, implantes, implantes dentários, prótese, prótese dentária, dentição, odontologia, clínica odontológica, clínica dentária, tratamento odontológico, dentista, tratamento dentário, dentista em Florianópolis, dentista em Curitiba." >
                <title> Clínicas odontológicas em Curitiba e Florianópolis </title>

        <?php    }else { ?>

                <META NAME="DESCRIPTION" CONTENT="Resgate seu sorriso com o PRID. Atendimento de excelência, alta tecnologia e soluções personalizadas em próteses e implantes dentários.">
                <META NAME="ABSTRACT" CONTENT="Apresentar a proposta de valor do PRID">
                <META NAME="KEYWORDS" CONTENT="PRID, Lessen Dental Clinic, implantes, implantes dentários, sorriso, dentes, higiene bucal, saúde bucal, reabilitação oral total avançada, implantes unitários, TID, prótese, prótese dentária, prótese fixa, prótese removível, dente, coroa, titânio, porcelana, dentição, odontologia, clínica odontológica, clínica dentária, tratamento odontológico, dentista, tratamento dentário." >
                <title> PRID | Referência em Próteses e Implantes Dentários </title>

        <?php    } ?>


        <!-- GOOGLE FONT-->
        <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i|Montserrat:400,700|Playfair+Display:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">

        <!-- Styles -->
        <link href="<?php base_url('style.css','css') ?>" rel="stylesheet" media="screen">


        
        <!-- magnific-popup -->
        <link href="<?php /*base_url('magnific-popup.css','css')*/ ?>" rel="stylesheet"> 

        
        
        <!-- ICONS ELEGANT FONT & FONT AWESOME & LINEA ICONS  -->  
        <link href="<?php /*base_url('icons-fonts.css','css')*/ ?>" rel="stylesheet">
        <link href="<?php /*base_url('ionicons.min.css','css')*/ ?>" rel="stylesheet">
        <link href="<?php /*base_url('font-awesome.min.css','css')*/ ?>" rel="stylesheet">

                
        <!-- ANIMATE --> 
        <link href="<?php /*base_url('animate.css','css')*/ ?>" rel="stylesheet">
        
		<!-- sweetalerts2 -->
        <link href="<?php base_url('sweetalert2.css','css') ?>" rel="stylesheet"> 
        <link href="<?php base_url('sweetalert2.min.css','css') ?>" rel="stylesheet"> 

        <!-- Custon css --> 
        <link href="<?php base_url('custom.css','css') ?>" rel="stylesheet">

        

        <!-- FORMULARIOS -->
        <link href="<?php base_url('formularios.css','css') ?>" rel="stylesheet">

        <!-- OWL -->
        <link href="<?php /*base_url('owl.carousel.css','css')*/ ?>" rel="stylesheet">
        <link href="<?php /*base_url('owl.transitions.css','css')*/ ?>" rel="stylesheet">

        <!-- SETTINGS -->
        <link href="<?php /*base_url('settings.css','css')*/ ?>" rel="stylesheet">


        <link href="<?php /*base_url('architecture.css','css')*/ ?>" rel="stylesheet">
        <link href="<?php /*base_url('freelance.css','css')*/ ?>" rel="stylesheet">
        <link href="<?php /*base_url('freelance-light.css','css')*/ ?>" rel="stylesheet">
        <link href="<?php /*base_url('hover.css','css')*/ ?>" rel="stylesheet">
        <link href="<?php /*base_url('interior.css','css')*/ ?>" rel="stylesheet">

        <!-- navigation -->
        <link href="<?php /*base_url('navigation.css','css')*/ ?>" rel="stylesheet"> 

        
        <!-- BOOTSTRAP -->
        <link href="<?php /*base_url('bootstrap.min.css','css')*/ ?>" rel="stylesheet"> 

        <!-- Icon css link -->
        <link href="<?php /*base_url('layers.css','css')*/ ?>" rel="stylesheet">


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-B8MNTLVDPW"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-B8MNTLVDPW');
        </script>


        <!-- Meta Pixel Facebook Code -->
        <script>
                !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '469868387966041');
                fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=469868387966041&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code --> 


             
    </head>
    
        
