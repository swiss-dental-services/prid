<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        

        <!-- jQuery  -->
        <script src="<?php base_url('jquery.min.js','js') ?>"></script>

        <!-- jQuery viewport -->
        <script src="<?php base_url('jquery.viewport.js','js') ?>" ></script>

        <!-- jQuerySimpleCounter -->
        <script src="<?php base_url('jQuerySimpleCounter.js','js') ?>" ></script>

        <!-- bootstrap -->
        <script src="<?php base_url('bootstrap.min.js','js') ?>"></script>  

        <script src="<?php base_url('smoothscroll.js','js') ?>" ></script>

        <!-- FORMS VALIDATION   -->
        <script src="<?php base_url('jquery.validate.min.js','js') ?>" ></script>

        <!-- WOW -->
        <script src="<?php base_url('wow.min.js','js') ?>" ></script>

        <!-- jquery ajaxchimp -->
        <script src="<?php base_url('jquery.ajaxchimp.min.js','js') ?>" ></script>

        <!-- jquery stellar -->
        <script src="<?php base_url('jquery.stellar.min.js','js') ?>" ></script>

        <!-- MAGNIFIC POPUP -->
        <script src="<?php base_url('jquery.magnific-popup.js','js') ?>"></script>

        <!-- OWL CAROUSEL -->    
        <script src="<?php base_url('owl.carousel.min.js','js') ?>" ></script>

        <!-- PORTFOLIO SCRIPTS -->
        <script src="<?php base_url('isotope.pkgd.min.js','js') ?>" ></script>
        <script src="<?php base_url('imagesloaded.pkgd.js','js') ?>" ></script>
        
        <!-- SLIDER REVOLUTION -->
        <script src="<?php base_url('jquery.themepunch.tools.min.js','js/rev-slider') ?>" ></script>   
        <script src="<?php base_url('jquery.themepunch.revolution.min.js','js/rev-slider') ?>" ></script>


        <!-- SLIDER REVOLUTION 5x EXTENSIONS -->
        <script src="<?php base_url('revolution.extension.actions.min.js','js/rev-slider') ?>" ></script>
        <script src="<?php base_url('revolution.extension.carousel.min.js','js/rev-slider') ?>" ></script>
        <script src="<?php base_url('revolution.extension.kenburn.min.js','js/rev-slider') ?>" ></script>
        <script src="<?php base_url('revolution.extension.layeranimation.min.js','js/rev-slider') ?>" ></script>
        <script src="<?php base_url('revolution.extension.migration.min.js','js/rev-slider') ?>" ></script>
        <script src="<?php base_url('revolution.extension.navigation.min.js','js/rev-slider') ?>" ></script>
        <script src="<?php base_url('revolution.extension.parallax.min.js','js/rev-slider') ?>" ></script>
        <script src="<?php base_url('revolution.extension.slideanims.min.js','js/rev-slider') ?>" ></script>
        <script src="<?php base_url('revolution.extension.video.min.js','js/rev-slider') ?>" ></script>



        <script src="<?php base_url('rev-slider-init.js','js') ?>" ></script>
        <script src="<?php base_url('interface.js','js') ?>" ></script>




        <!--jQuery Mask   -->
        <script src="<?php base_url('jquery.mask.min.js','js') ?>"></script>

        <!--Sweet Alert   -->
        <script src="<?php base_url('sweetalert2.all.min.js','js') ?>"></script>

        <!--FORMULARIO JOGO QUIZ   -->
        <script src="<?php base_url('formulario_quiz.js','js') ?>"></script>


        <!-- FORMULARIOS GET LEADS -->
        <script src="<?php base_url('formularios_get_leads.js','js') ?>" ></script>


            


        

        <script type="text/javascript">

          function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (30*24*60*60*1000));
                var expires = "expires="+ d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
          }

          function getCookie(cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for(var i = 0; i <ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function reloadCookie(_var) {
                setCookie("lang",_var,30);
                setTimeout(() => {
                    location.reload();
                }, 500);
            }      

            
            function reload(){

                
                $('html,body').animate({scrollTop:0}, 100);
                window.location.reload();
            }

         
        
        function onlynumber(evt) {
             var theEvent = evt || window.event;
             var key = theEvent.keyCode || theEvent.which;
             key = String.fromCharCode( key );
             
             var regex = /^[0-9.]+$/;
             if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
             }
        }           	
        	
        </script>

        
        
        <script>

           $(document).ready(function() {

             /*alert("document ready.............");*/
             



          });

           
        /**
        *
        * @param {String} id Id do formulário
        * @param {String} cta Texto para o CTA do formulário.
        * @param {String} thanks Thank Page do site, colocar FALSE caso não queira utilizar.
        * @param {Array} extra Array de objetos com campos extras que serão adicionados ao formulário.
        * @param {Array} data Array de objetos com cada campo que existirá no formulário.
        */
        var existForm = document.getElementById("FORM-PRID");
                
        </script>
    
        <script type="text/javascript">
          
          var estado = document.getElementById("phone");
          if( existForm ){
              estado.onkeypress = function(evt){


                 var theEvent = evt || window.event;
                 var key = theEvent.keyCode || theEvent.which;
                 key = String.fromCharCode( key );
                 
                 var regex = /^[0-9.]+$/;
                 if( !regex.test(key) ) {
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault) theEvent.preventDefault();
                 }

                
              };
          }

          $("#phone").mask("(99) 99999-9999");


          var existFormSuporte = document.getElementById("form_suporte_fale_connosco");
          if( existFormSuporte ){

              /*var estado_2 = document.getElementById("telefone_fale_connosco");
              estado_2.onkeypress = function(evt){


                 var theEvent = evt || window.event;
                 var key = theEvent.keyCode || theEvent.which;
                 key = String.fromCharCode( key );
                 
                 var regex = /^[0-9.]+$/;
                 if( !regex.test(key) ) {
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault) theEvent.preventDefault();
                 }

                
              };*/

              //$("#telefone_fale_connosco").mask("(99) 99999-9999");
          }




          var btnForm = document.getElementById("btn-form-recrutamento");
          if( btnForm ){           
           

          }

          /* --------------------------------------------
            SMOOTH SCROLL TO 
          --------------------------------------------- */
          $('a.smooth-scroll[href^="#"]').on('click', function(event) {

            var target = $( $(this).attr('href') );

            if( target.length ) {
              event.preventDefault();
              $('html, body').animate({
                scrollTop: target.offset().top - 80
              }, 600);
            }   

          });

          $(window).scroll(function (event) {
             var scroll = $(window).scrollTop();
                 
                 if(scroll>=200){

                      $("div a img.style-img-menu").attr('src', '<?php base_url('logo_prid-01.svg','img/custon') ?>');
                     

                 }else{

                      $("div a img.style-img-menu").attr('src', '<?php base_url('logo_prid-02.svg','img/custon') ?>');
                 
                 }
             
          }); 

        </script>

        <script>
          var chatbox = document.getElementById('fb-customer-chat');
          chatbox.setAttribute("page_id", "102009648715964");
          chatbox.setAttribute("attribution", "biz_inbox");

          window.fbAsyncInit = function() {
            FB.init({
              xfbml            : true,
              version          : 'v12.0'
            });
          };

          (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/pt_BR/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
        </script>