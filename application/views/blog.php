<!DOCTYPE html>
    <html lang="pt-br">
      <!-- Start Headers Includes ===========
      ======================================= -->
      <?php
              require_once 'includes/generateAllInfo.php';
              
        include('helper.php');
        
        include('header.php');
        
      ?>
      <body class="bg-page">

        <!-- LOADER --> 
      <div class="loader">
        <div class="spinner">
          <div class="double-bounce1"></div>
          <div class="double-bounce2"></div>
        </div>
      </div>

      <?php 
          include('nav/bar-nav.php');
                
          include('blog/banner.php'); 
      ?>

        <div class="content">
                  
        <!-- ==================================
        ================== End Headers Includes -->
        <!-- Start Page Struture ==============
        ======================================= -->
        <?php include('blog/structure.php'); ?>
        <!-- ==================================
        ===================== End Page Struture --> 
        <!-- Start Footers Includes ===========
        ======================================= -->
        <?php include('footer.php');  ?>
        
      </div>
      <?php include('commum-js.php'); ?>
        <!-- ==================================
        ================== End Footers Includes -->

      </body>
    </html>
    <script type="text/javascript">
    	
    	//-----------------------------paginação do blog--------------------

            var numberOfItems       = $('#listado .lista').length;
            var numberOfItemsFiltro = 0;
            var limitPerPages       = 10;
            $('#listado .lista:gt('+(limitPerPages - 1)+')').hide();
            var totalPages          = Math.ceil( numberOfItems / limitPerPages );
            var index               = 1;
            
            //alert(numberOfItems);
            var posicion = $("#listado").offset().top;

            if(totalPages == 1){//para ocultar a paginação quando seja uma pagina só

                $('#pagination-blog').hide();

            }

            if(numberOfItems == 0){//para ocultar a paginação quando não exista nenhuma publicão para mostrar 

                $('#pagination-blog').hide(); 

            }else{

              $('h2.show-sem-cat').addClass('display-none');
            }
            

            $('#prev').addClass('noHover');

                               

            $('#next').on('click', function(){
              
                  
                  var currentPage = index;

                  if(currentPage === totalPages){
                    
                    return false;

                  }else{

                    currentPage++;
                    
                    $('#listado .lista').hide();
                    
                  }
                  var grandTotal = limitPerPages * currentPage;

                  for (var i = grandTotal - limitPerPages; i < grandTotal; i++) {

                    $('#listado .lista:eq('+ i +')').show();

                  }

                 
                    if(currentPage === totalPages){

                        $('#next').addClass('noHover');

                    }else{

                        $('#next').removeClass('noHover');

                    }
                    if(currentPage === 1){

                        $('#prev').addClass('noHover');

                    }else{

                        $('#prev').removeClass('noHover');

                    }
                  
                  index++;
                  $("html, body").animate({
                        scrollTop: posicion - 150
                    }, 400);
             

            });

            $('#prev').on('click', function(){
              

                  var currentPage = index;

                  if(currentPage === 1){

                    return false;

                  }else{

                    currentPage--;
                    
                    $('#listado .lista').hide();

                  }
                  var grandTotal = limitPerPages * currentPage;

                  for (var i = grandTotal - limitPerPages; i < grandTotal; i++) {

                    $('#listado .lista:eq('+ i +')').show();

                  }

                 
                    if(currentPage === 1){

                        $('#prev').addClass('noHover');

                    }else{

                        $('#prev').removeClass('noHover');

                    }

                    if(currentPage === totalPages){

                        $('#next').addClass('noHover');

                    }else{

                        $('#next').removeClass('noHover');

                    }
                  
                  index--;
                  $("html, body").animate({
                        scrollTop: posicion - 150
                    }, 400);
              

            });

    </script>