<!--****************************************************** 1ra sessao  **********************************************************************-->
        

<section class="section clearfix pt-2em wow fadeInUp" style="/*padding-top: 3.1em;*/">
  <div class="left-fluid-content col-sm-12 col-md-6 col-lg-4 col-lg-offset-2">
    <?php
      echo funGetAdvancedBanners('clinicas_one', '

        <header class="text-center">
          <h2 class="text-left section-title-3">{{title}}</h2>
        </header>
        <div class="about-entry">
          <p>{{text}}</p>
          <p>{{subtext}}</p>
          <p style="color: #e7344c;; font-weight: bold;">{{subtitle}}</p>
        </div>

        <div class="row">
          <header class="text-center col-md-8 col-md-offset-2- mt-50" style="display: flex;">
            <a href="{{callAction}}" data-toggle="modal-" class="btn btn-violet- btn-red wow swing smooth-scroll" style="visibility: visible; animation-name: swing;">{{callTitle}}</a>
          </header>
        </div>       

      ');
    ?>
    
  </div>
  <div class="bg-aside-1- bg-aside-clinicas bg-right-fluid col-md-6 hidden-sm hidden-xs" style="transform: translate(-119px, 41px);"></div>
</section>

