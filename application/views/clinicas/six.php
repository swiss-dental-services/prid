
   <!--****************************************************** 6ta sessao Depoimentos **************************************************************************--> 

<section class="clients bg-light- section">
  <div class="container">
      <div class="row">
        <header class="text-center col-md-8 col-md-offset-2">
        <h2 class="section-title"><?=home_six_text_1?></h2>
        <!--<p class="section-lead">Our team of experts has competences of different<br> branches and specializations.</p> -->
        </header>
      </div>
    </div>
    <div class="section-content">
      <div class="container-fluid">
        <div class="col-md-6 col-md-offset-3">
          <div class="clients-wrapper">
            <div class="client-carousel js-client-carousel">
            <?php
                echo funGetSlide('home_six','','','

                 <div class="client">
                    <span class="quote">"</span>
                    <div style="display: flex; justify-content: center;" class="OnlyDesktop">
                      <img src="{{img}}" style="width: 50%; margin-bottom: 30px;">
                    </div>
                    <p class="client-text">{{text}}</p>
                    <span class="h5 client-name">{{title}}   —   </span>
                    <i>{{subtitle}}</i>

                    <div class="wow- fadeInRight- move-div-1" style="display: flex; justify-content: start; align-items: flex-end;">

                      <a href="http://www.youtube.com/watch?v=ANwf8AE3_d0" class="v2- js-video-play" data-wow-delay="0.6s"><img class="img-play" src="assets/img/custon/play-01.svg"></a>   

                      <p class="client-text move-title">ASSISTA AO VÍDEO</p>
                    </div>

                 </div>
                
                ');
            ?>
              
            </div>
          </div>
        </div>
      </div>
    </div>
</section>