<!--****************************************************** contact  *****************************************************************************-->
<section id="formulario" class="section section-button-" style="background-color: #e31b1c;">
    
    <div class="page-section">
        <div class="container">
            <div class="row">
                
                <div class="col-md-6 right-50 wow fadeInLeft- OnlyDesktop" style="position: relative;">
                            
                    <div class="ads-img-cont" >
                        <img src="<?php base_url('img_formulario.png','img/custon') ?>" alt="img" style="margin-top: 0%; max-width: 225%; position: absolute; transform: translate(-91px, -58px);">
                    </div>
    
                </div>


                <div class="col-md-6 left-50 right-desktop" style="display: flex; justify-content: center; align-items: center; flex-direction: column;">
                    <div class="title-fs-45 uppercase mt-50 mb-50"></div>

                    <div class="with-content" id="form-home-TSS"></div>


                </div>
  
  
            </div>
        </div>
    </div>    
    
</section>

