<!--****************************************************** 2da sessao  *************************************************************-->

<section class="mission-1 section" style="background: #e7344c;">
  <div class="left-fluid-content col-sm-12 col-md-6 col-lg-4 col-lg-offset-2 address-panel-custon">
    
    <div class="address-item-2">
      <h2 style="color: #fff;">Florianópolis - SC</h2>
    </div>

    <div class="address-item-2">
      <i class="icon ion-iphone" style="float: left; font-size: 2em; width: 1.6em; line-height: 1;"></i>
      <div class="address-title-2">(48) 98844-7887</div>
    </div>
    <!--<div class="address-item-2">
      <i class="icon ion-ios-email-outline" style="float: left; font-size: 2em; width: 1.6em; line-height: 1;"></i>
      <div class="address-title-2">info@virgo.com</div>
    </div>-->
    <div class="address-item-2">
      <i class="icon ion-ios-location-outline"></i>
      <div class="address-title-2">Rua Dom Jaime Câmara, Nº 170<br>Sala 701 - Centro</div>
    </div>
    <!--<ul class="social-list">
      <li><a href="" class="fa fa-facebook"></a></li>
      <li><a href="" class="fa fa-twitter"></a></li>
      <li><a href="" class="fa fa-linkedin"></a></li>
      <li><a href="" class="fa fa-instagram"></a></li>
    </ul> -->
    <p class="address-info">
      Nossa unidade fica localizada na região central da capital catarinense, conta com equipamentos modernos e dispõe de uma estrutura que oferece conforto e uma experiência única para nossos pacientes. 
    </p>


  </div>
  <div class="bg-clinica-floripa bg-right-fluid col-md-6 hidden-sm hidden-xs"></div>
</section>



<section class="mission-2 section"  style="background: #e7344c; /*margin-top: 20px; margin-bottom: 20px;*/">
  <div class="bg-clinica-curitiba bg-left-fluid col-md-6 hidden-sm hidden-xs"></div>

  <div class="right-fluid-content col-md-6 col-lg-4 col-md-offset-6 address-panel-custon-2">
    
    <div class="address-item-2">
      <h2 style="color: #fff;">Curitiba - PR</h2>
    </div>

    <div class="address-item-2">
      <i class="icon ion-iphone" style="float: left; font-size: 2em; width: 1.6em; line-height: 1;"></i>
      <div class="address-title-2">(41) 98844-7887</div>
    </div>
    <!--<div class="address-item-2">
      <i class="icon ion-ios-email-outline" style="float: left; font-size: 2em; width: 1.6em; line-height: 1;"></i>
      <div class="address-title-2">info@virgo.com</div>
    </div>-->
    <div class="address-item-2">
      <i class="icon ion-ios-location-outline"></i>
      <div class="address-title-2">Rua Rocha Pombo, Nº 650<br>2º andar - Juveve</div>
    </div>
    <!--<ul class="social-list">
      <li><a href="" class="fa fa-facebook"></a></li>
      <li><a href="" class="fa fa-twitter"></a></li>
      <li><a href="" class="fa fa-linkedin"></a></li>
      <li><a href="" class="fa fa-instagram"></a></li>
    </ul>-->
    <p class="address-info">
      Localizada próxima ao Centro Cívico, nossa unidade em Curitiba oferece para nossos pacientes o que há de melhor em equipamentos e infraestrutura, para atender com qualidade às suas necessidades.
    </p>

  </div>
  
</section> 


<!--<section class="mission-2 section">

  <div class="content"> 
      <section class="contacts">
        <div class="map-wrapper">
          <div id="map-" class="map" style="background: url(assets/img/custon/imagem_florianopolis.jpg) 50% 0 no-repeat; margin-left: 320px;"></div>
          <div class="col-md-5 col-lg-4 address-panel-2">
            <div class="address-item-2">
              <i class="icon ion-iphone"></i>
              <div class="address-title-2">+8 800 555 35 35</div>
            </div>
            <div class="address-item-2">
              <i class="icon ion-ios-email-outline"></i>
              <div class="address-title-2">info@virgo.com</div>
            </div>
            <div class="address-item-2">
              <i class="icon ion-ios-location-outline"></i>
              <div class="address-title-2">Beechwood Dr, Lawrence,<br>NY 11559, USA</div>
            </div>
            <ul class="social-list">
              <li><a href="" class="fa fa-facebook"></a></li>
              <li><a href="" class="fa fa-twitter"></a></li>
              <li><a href="" class="fa fa-linkedin"></a></li>
              <li><a href="" class="fa fa-instagram"></a></li>
            </ul>
            <i class="address-info">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. 
            </i>
          </div>
        </div>
        
      </section>
      
      
    </div>
  
</section>


<section class="mission-2 section">

  <div class="content"> 
      <section class="contacts" style="height: 42em;">
        <div class="map-wrapper">
          <div id="map-" class="map" style="background: url(assets/img/custon/imagem_curitiba.jpg) 50% 0 no-repeat; /*margin-left: 320px;*/position: absolute; right: 317px; height: 50em;"></div>
          <div class="col-md-5 col-lg-4 address-panel-custon" style="position: absolute; right: 0; z-index: 1; top: 0; padding: 5% 7%; background-color: rgba(101,52,255,0.9); color: #fff; height: 50em;">
            <div class="address-item-2">
              <i class="icon ion-iphone" style="float: left; font-size: 2em; width: 1.6em; line-height: 1;"></i>
              <div class="address-title-2">+8 800 555 35 35</div>
            </div>
            <div class="address-item-2">
              <i class="icon ion-ios-email-outline" style="float: left; font-size: 2em; width: 1.6em; line-height: 1;"></i>
              <div class="address-title-2">info@virgo.com</div>
            </div>
            <div class="address-item-2">
              <i class="icon ion-ios-location-outline" style="float: left; font-size: 2em; width: 1.6em; line-height: 1;"></i>
              <div class="address-title-2">Beechwood Dr, Lawrence,<br>NY 11559, USA</div>
            </div>
            <ul class="social-list">
              <li><a href="" class="fa fa-facebook"></a></li>
              <li><a href="" class="fa fa-twitter"></a></li>
              <li><a href="" class="fa fa-linkedin"></a></li>
              <li><a href="" class="fa fa-instagram"></a></li>
            </ul>
            <i class="address-info">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. 
            </i>
          </div>
        </div>
        
      </section>
      
      
    </div>
  
</section>-->


<!--<section class="section clearfix" style="background-color: #f7f7f7;">
  <div class="left-fluid-content col-sm-12 col-md-6 col-lg-4 col-lg-offset-2">
    <?php
      /*echo funGetAdvancedBanners('home_six', '

        <header class="text-center">
          <h2 class="text-left section-title-3">{{title}}</h2>
        </header>
        <div class="about-entry">
          <p>{{text}}</p>
        </div>

        <!--<div class="row">
          <header class="text-center col-md-8 col-md-offset-2- mt-50" style="display: flex;">
            <a href="{{callAction}}" data-toggle="modal-" class="btn btn-violet- btn-red wow swing" style="visibility: visible; animation-name: swing;">{{callTitle}}</a>
          </header>
        </div> -->       

      ');*/
    ?>
    
  </div>
  <div class="bg-aside-1- bg-aside-home bg-right-fluid col-md-6 hidden-sm hidden-xs"></div>
</section> -->