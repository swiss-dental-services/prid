<!-- Header -->

    <header class="navbar">
      <div>
        <a href="<?php base_url('') ?>" class="brand js-target-scroll">
          <img src="<?php base_url('logo_prid-02.svg','img/custon') ?>" class="style-img-menu">          
        </a>

        <!-- Navbar Collapse -->

          <button type="button" class="navbar-toggle collapsed ajust-move" data-toggle="collapse" data-target="#nav-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

        <ul class="social-list OnlyDesktop">
          <li><a href="http://facebook.com/PRID-Brasil-Programa-Regional-de-Implantes-Dent%C3%A1rios-102009648715964" target="_blank" class="fa fa-facebook"></a></li>
          <li><a href="https://twitter.com/pridbrasil" target="_blank" class="fa fa-twitter"></a></li>
          <li><a href="https://www.youtube.com/channel/UCK6HJUJ1pqqxUuys9VjKlQg" target="_blank" class="fa fa-youtube"></a></li>
          <li><a href="https://www.instagram.com/prid_brasil/" target="_blank" class="fa fa-instagram"></a></li>
        </ul>  
      
        <!-- Navigation Desctop -->

        <nav class="nav-desctop hidden-xs hidden-sm" style="/*transform: translateY(20px);*/">
          <ul class="nav-desctop-list" style="padding-top: 22px;">
            <!--<li class="<?php echo ($active=='home') ? 'active' : '';?>"><a href="<?php /*base_url('')*/ ?>">Home</a></li>-->
            <li class="<?php echo ($active=='so') ? 'active' : '';?>"><a href="<?php base_url('solucoes-oferecidas') ?>">Soluções Oferecidas</a></li>
            <li class="<?php echo ($active=='qs') ? 'active' : '';?>"><a href="<?php base_url('quem-somos') ?>">Quem Somos</a></li>
            <li class="<?php echo ($active=='tra') ? 'active' : '';?>"><a href="<?php base_url('tratamentos') ?>">Tratamentos</a></li>
            <li class="<?php echo ($active=='blog') ? 'active' : '';?>"><a href="<?php base_url('blog') ?>">Blog</a></li>
            <li class="<?php echo ($active=='cli') ? 'active' : '';?>"><a href="<?php base_url('clinicas') ?>">Clínicas</a></li>
          </ul>
        </nav>

        <!-- Navigation Mobile -->

        <nav class="nav-mobile hidden-md hidden-lg">
          <div class="collapse navbar-collapse" id="nav-collapse"> 
            <ul class="nav-mobile-list">
              <!--<li class="<?php echo ($active=='home') ? 'active' : '';?>"><a href="<?php /*base_url('')*/ ?>">Home</a></li>-->
              <li class="<?php echo ($active=='so') ? 'active' : '';?>"><a href="<?php base_url('solucoes-oferecidas') ?>">Soluções Oferecidas</a></li>
              <li class="<?php echo ($active=='qs') ? 'active' : '';?>"><a href="<?php base_url('quem-somos') ?>">Quem Somos</a></li>
              <li class="<?php echo ($active=='tra') ? 'active' : '';?>"><a href="<?php base_url('tratamentos') ?>">Tratamentos</a></li>
              <li class="<?php echo ($active=='blog') ? 'active' : '';?>"><a href="<?php base_url('blog') ?>">Blog</a></li>
              <li class="<?php echo ($active=='cli') ? 'active' : '';?>"><a href="<?php base_url('clinicas') ?>">Clínicas</a></li>
            </ul>
          </div>
        </nav> 
      </div>
    </header>